<div class="row">
	<h1 class="page-header">
		Subcategory
	</h1>

	<div class="row main">
		<table class="table table-condensed">
			<tr>
				<th>action</th>
				<th>id</th>
				<th>slug</th>
				<th>name</th>
				<th>Category ID</th>
				<th>audit status</th>
				<th>created at</th>
				<th>updated_at</th>
			</tr>
			@foreach($subcategories as $subcategory)
				<!--tampilin post-->
				<tr>
					<td nowrap>
						<div class="btn-group btn-group-xs">
							{!! HTML::link('admin/updateSubCategory/'.$subcategory->id,'',array('class'=>'glyphicon glyphicon-pencil')) !!}
							{!! HTML::link('admin/doDeleteSubCategory/'.$subcategory->id,'',array('class'=>'glyphicon glyphicon-minus')) !!}
						</div>
					</td>
					<td>{{$subcategory->id}}</td>
					<td>{{$subcategory->slug}}</td>
					<td>{{$subcategory->name}}</td>
					<td>{{$subcategory->category_id}}</td>
					<td>{{$subcategory->audit_status}}</td>
					<td>{{$subcategory->created_at}}</td>
					<td>{{$subcategory->updated_at}}</td>
				</tr>
			@endforeach
		</table>
	</div>
	<div class="row">
		{!! HTML::link('admin/insertSubCategory/','insert',array('class'=>'btn btn-default')) !!}
	</div>
</div>