@extends('admin::_layout.base')

@section('content')
		@include('admin::_partials.header')
		<div class="container-fluid admin-content">
			<div class="row">
				@include('admin::_partials.menu')
				<div id="page-wrapper" class="col-sm-9 col-md-10">
					@yield('innerContent')
				</div>
			</div>
		</div>
@stop