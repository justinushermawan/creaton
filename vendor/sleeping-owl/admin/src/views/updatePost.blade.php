@extends('admin::_layout.base')

@include('admin::_partials.header')

@section('content')


<div class="container">
	<div class="row">
		<h1 class="page-header">Update Post</h1>
		{!! Form::open(array('url'=>'admin/doUpdatePost/'.$post->id,'class'=>'form-horizontal')) !!}
			<div class="form-group">
				<label for="" class="control-label col-md-2">slug</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="slug" id="" value='{{ $post->slug }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">user</label>
				<div class="col-md-10">
					{!! Form::select('user_id',$user) !!}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2" name="posttype">post type</label>
				<div class="col-md-10">
					{!! Form::select('posttype',$posttype) !!}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2"  name="subcategory">sub category</label>
				<div class="col-md-10">
					{!! Form::select('subcategory',$subcategory) !!}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">title</label>
				<div class="col-md-10">
					<input type="text" name="title" class="form-control" id="" value='{{ $post->title }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">message</label>
				<div class="col-md-10">
					<input type="text" name="message" class="form-control" id="" value='{{ $post->message }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">file path</label>
				<div class="col-md-10">
					<input type="text" name="filepath" class="form-control" id=""value='{{ $post->file_path }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">video url</label>
				<div class="col-md-10">
					<input type="text" name="video_url" class="form-control" id="" value='{{ $post->video_url }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">tags</label>
				<div class="col-md-10">
					<input type="text" name="tags" id="" class="form-control" value='{{ $post->tags}}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">allow comment</label>
				<div class="col-md-10">
					<input type="text" name="allowcomment" class="form-control" id="" value='{{ $post->allow_comment }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">nsfw</label>
				<div class="col-md-10">
					<input type="text" name="nsfw" id="" class="form-control" value='{{ $post->nsfw }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">reputation</label>
				<div class="col-md-10">
					<input type="number" name="reputation" class="form-control" value='{{ $post->reputation }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">total views</label>
				<div class="col-md-10">
					<input type="number" name="totalview" class="form-control" value='{{ $post->total_views }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">audit status</label>
				<div class="col-md-10">
					<input type="text" name="audit_status" class="form-control" id="" value='{{ $post->audit_status }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">created at</label>
				<div class="col-md-10">
					<input type="date" name="createdat" class="form-control" id="" value='{{ $post->created_at }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">updated at</label>
				<div class="col-md-10">
					<input type="date" name="updatedat" class="form-control" id="" value='{{ $post->updated_at }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2"></label>
				<div class="col-md-10">
					{!! Form::submit('udpate',['class' => 'btn btn-success']) !!}
					{!! HTML::link('admin/Posts','Cancel',['class' => 'btn btn-danger']) !!}
				</div>
			</div>
			
		{!! Form::close() !!}
	</div>
</div>

@stop