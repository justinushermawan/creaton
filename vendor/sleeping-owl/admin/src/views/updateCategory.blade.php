@extends('admin::_layout.base')

@include('admin::_partials.header')

@section('content')

<div class="container">
	<div class="row">
		<h1 class="page-header">
			Update Category
		</h1>
		{!! Form::open(array('url'=>'admin/doUpdateCategory/'.$category->id,'class'=>'form-horizontal')) !!}
			<div class="form-group">
				<label for="" class="control-label col-md-2">slug</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="slug" id="" value='{{ $category->slug }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">name</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="name" id="" value='{{ $category->name }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">audit status</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="audit_status" id="" value='{{ $category->audit_status }}'>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2"></label>
				<div class="col-md-10">
					{!! Form::submit('update',['class'=> 'btn btn-success']) !!}
					{!! HTML::link('admin/Categories','Cancel',['class' => 'btn btn-danger']) !!}
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>

@stop