@extends('admin::_layout.base')

@include('admin::_partials.header')

@section('content')

<div class="container">
	<div class="row">
		<h1 class="page-header">Update Post</h1>
		{!! Form::open(array('url'=>'admin/doInsertPost','class'=>'form-horizontal')) !!}
			<div class="form-group">
				<label for="" class="control-label col-md-2">slug</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="slug" id="" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">user</label>
				<div class="col-md-10">
					{!! Form::select('user_id',$user) !!}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2" name="posttype">post type</label>
				<div class="col-md-10">
					{!! Form::select('posttype',$posttype) !!}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2"  name="subcategory">sub category</label>
				<div class="col-md-10">
					{!! Form::select('subcategory',$subcategory) !!}
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">title</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="title" id="" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">message</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="message" id="" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">file path</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="filepath" id=""value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">video url</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="video_url" id="" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">tags</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="tags" id="" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">allow comment</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="allowcomment" id="" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">nsfw</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="nsfw" id="" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">reputation</label>
				<div class="col-md-10">
					<input type="number" class="form-control" name="reputation" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">total views</label>
				<div class="col-md-10">
					<input type="number" class="form-control" name="totalview" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">audit status</label>
				<div class="col-md-10">
					<input type="text" class="form-control" name="audit_status" id="" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">created at</label>
				<div class="col-md-10">
					<input type="date" class="form-control" name="createdat" id="" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2">updated at</label>
				<div class="col-md-10">
					<input type="date" class="form-control" name="updatedat" id="" value=''>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-md-2"></label>
				<div class="col-md-10">
					{!! Form::submit('Insert', ['class' => 'btn btn-success']) !!}
					{!! HTML::link('admin/Posts','Cancel',['class' => 'btn btn-danger']) !!}
				</div>
			</div>
			
			
		{!! Form::close()!!}
	</div>
</div>
@stop