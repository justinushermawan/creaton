<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ Admin::instance()->router->routeHome() }}">Creaton Admin Panel</a>
		</div>
		<div class="navbar">
			<ul class="nav navbar-right">
				<li class="">
					<ul class="">
						<li><a href="{{ Admin::instance()->router->routeToAuth('logout') }}"><i class="fa fa-sign-out fa-fw fa-lg"></i></a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>


