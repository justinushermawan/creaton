<div class="col-sm-3 col-md-2 sidebar" role="navigation">
	<ul class="nav nav-sidebar">
		@foreach ($menu as $item)
			<li>{!! $item->render() !!}</li>
		@endforeach
	</ul>
</div>