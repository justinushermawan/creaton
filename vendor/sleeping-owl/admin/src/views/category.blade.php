<div class="row">
	<h1 class="page-header">
		Category
	</h1>

	<div class="row main">
		<table class="table table-condensed">
			<tr>
				<th>action</th>
				<th>id</th>
				<th>slug</th>
				<th>name</th>
				<th>audit status</th>
				<th>created at</th>
				<th>updated_at</th>
			</tr>
			@foreach($categories as $category)
				<!--tampilin post-->
				<tr>
					<td nowrap>
						<div class="btn-group btn-group-xs">
							{!! HTML::link('admin/updateCategory/'.$category->id,'',array('class'=>'glyphicon glyphicon-pencil')) !!}
							{!! HTML::link('admin/doDeleteCategory/'.$category->id,'',array('class'=>'glyphicon glyphicon-minus')) !!}
						</div>
					</td>
					<td>{{$category->id}}</td>
					<td>{{$category->slug}}</td>
					<td>{{$category->name}}</td>
					<td>{{$category->audit_status}}</td>
					<td>{{$category->created_at}}</td>
					<td>{{$category->updated_at}}</td>
				</tr>
			@endforeach
		</table>
	</div>
	<div class="row">
		{!! HTML::link('admin/insertCategory/','insert',array('class'=>'btn btn-default')) !!}
	</div>
</div>