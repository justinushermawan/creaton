	
	<div class="row">
		<h1 class="page-header">
			Post
		</h1>

		<div class="btn-group">
			@foreach($category as $key=>$value)
				<?= HTML::link('admin/Posts/'.$key,$value,array('class'=>'btn btn-info')) ?>
			@endforeach
		</div>
	</div>
	<div class="row main">
		<table class="table table-condensed">
			<tr>
				<th>action</th>
				<th>id</th>
				<th>slug</th>
				<th>user_id</th>
				<th>post_type_id</th>
				<th>subcategory_id</th>
				<th>title</th>
				<th>message</th>
				<th>file_path</th>
				<th>video_url</th>
				<th>tags</th>
				<th>allow_comment</th>
				<th>nsfw</th>
				<th>reputation</th>
				<th>total_views</th>
				<th>audit_status</th>
				<th>created_at</th>
				<th>udpated_at</th>
			</tr>
			@foreach($posts as $post)
				<!--tampilin post-->
				<tr>
					<td nowrap>
						<div class="btn-group btn-group-xs">
							{!! HTML::link('admin/updatePost/'.$post->id,'',array('class'=>'glyphicon glyphicon-pencil')) !!}
							{!! HTML::link('admin/doDeletePost/'.$post->id,'',array('class'=>'glyphicon glyphicon-minus')) !!}
						</div>
					</td>
					<td>{{$post->id}}</td>
					<td>{{$post->slug}}</td>
					<td>{{$post->user_id}}</td>
					<td>{{$post->post_type_id}}</td>
					<td>{{$post->subcategory_id}}</td>
					<td>{{$post->title}}</td>
					<td>{{$post->message}}</td>
					<td>{{$post->file_path}}</td>
					<td>{{$post->video_url}}</td>
					<td>{{$post->tags}}</td>
					<td>{{$post->allow_comment}}</td>
					<td>{{$post->nsfw}}</td>
					<td>{{$post->reputation}}</td>
					<td>{{$post->total_views}}</td>
					<td>{{$post->audit_status}}</td>
					<td>{{$post->created_at}}</td>
					<td>{{$post->updated_at}}</td>
				</tr>
			@endforeach
		</table>
		
	</div>
	<div class="row">
		{!! HTML::link('admin/insertPost','insert',array('class'=>'btn btn-default')) !!}	
	</div>