<?php namespace SleepingOwl\Admin\Controllers;
use Illuminate\Routing\Controller;
use View;
use App\Category;
use App\Post;
use App\PostType;
use App\SubCategory;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Input;
use Redirect;

class SubCategoryController extends Controller
{
	
	public function index(){
		$subcategory = SubCategory::all();
		return View('admin::subcategory',[
			'subcategories' => $subcategory
		]);

	}

	public function updateSubCategory($id){
		$category = Category::all();
		$subcategory = SubCategory::find($id);
		foreach ($category as $temp) {
			$category_final[$temp->id] = $temp->name;
		}
		return View('admin::updateSubCategory',[
				'subcategory' 	=>$subcategory,
				'category'	=>$category_final,
				'pageTitle'		=> 'Update Sub Category'
		]);
	}

	public function doUpdateSubCategory($id){
		$input = Input::all();
		//get current time + 7 for JKT
		$time = date('Y-m-d H:i:s',time()+7*60*60);

		$subcategory = SubCategory::find($id);
		$subcategory->slug = $input['slug'];
		$subcategory->name = $input['name'];
		$subcategory->category_id = $input['category'];
		$subcategory->audit_status = "U";
		$subcategory->updated_at = $time;
		$subcategory->save();
		return Redirect::to('/admin/Sub_Categories');
	}

	public function insertSubCategory(){
		$category = Category::all();
		foreach ($category as $temp) {
			$category_final[$temp->id] = $temp->name;
		}
		return View('admin::insertSubCategory',[
			'category' => $category_final,
			'pageTitle'		=> 'Insert New SubCategory'
		]);
	}

	public function doInsertSubCategory(){
		$input = Input::all();
		$subcategory = new SubCategory;
		//get current time + 7 for JKT
		$time = date('Y-m-d H:i:s',time()+7*60*60);
		$subcategory->slug = $input['slug'];
		$subcategory->name = $input['name'];
		$subcategory->category_id = $input['category'];
		$subcategory->audit_status = "I";
		$subcategory->created_at = $time;
		$subcategory->updated_at = '0000-00-00 00:00:00';
		$subcategory->save();
		return Redirect::to('/admin/Sub_Categories');
	}

	public function doDeleteSubCategory($id){
		$subcategory = SubCategory::find($id);
		$subcategory->audit_status = 'D';
		$subcategory->save();
		return Redirect::to('/admin/Sub_Categories');
	}	

}


?>