<?php namespace SleepingOwl\Admin\Controllers;
use Illuminate\Routing\Controller;
use View;
use App\Category;
use App\Post;
use App\PostType;
use App\SubCategory;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Input;
use Redirect;

class CategoryController extends Controller
{
	
	public function index(){
		$category = Category::all();
		return View('admin::category',[
			'categories' => $category
		]);

	}

	public function updateCategory($id){
		$category = Category::find($id);
		return View('admin::updateCategory',[
				'category' 	=>$category,
				'pageTitle'		=> 'Update Category'
		]);
	}

	public function doUpdateCategory($id){
		$input = Input::all();
		//get current time + 7 for JKT
		$time = date('Y-m-d H:i:s',time()+7*60*60);

		$category = Category::find($id);
		$category->slug = $input['slug'];
		$category->name = $input['name'];
		$category->audit_status = $input['audit_status'];
		$category->updated_at = $time;
		$category->save();
		return Redirect::to('/admin/Categories');
	}

	public function insertCategory(){

		return View('admin::insertCategory',[
			'pageTitle'		=> 'Insert New Category'
		]);
	}

	public function doInsertCategory(){
		$input = Input::all();
		$category = new Category;
		//get current time + 7 for JKT
		$time = date('Y-m-d H:i:s',time()+7*60*60);
		$category->slug = $input['slug'];
		$category->name = $input['name'];
		$category->audit_status = "I";
		$category->created_at = $time;
		$category->updated_at = '0000-00-00 00:00:00';
		$category->save();
		return Redirect::to('/admin/Categories');
	}

	public function doDeleteCategory($id){
		$category = Category::find($id);
		$category->audit_status = 'D';
		$category->save();
		return Redirect::to('/admin/Categories');
	}	

}


?>