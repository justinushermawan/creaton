<?php namespace SleepingOwl\Admin\Controllers;
use Illuminate\Routing\Controller;
use View;
use App\Category;
use App\Post;
use App\PostType;
use App\SubCategory;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Input;
use Redirect;

class PostReportController extends Controller
{
	
	public function index(){
		$post = Post::all();
		
		
		return View('admin::post',[
			'posts' => $post,
			'category' => $category_final
		]);

	}

	public function updatePost($id){
		$category = Category::all();
		$subcategory = SubCategory::all();
		$posttype = PostType::all();
		$post = Post::find($id);
		$user = User::all();
		foreach ($category as $temp) {
			$category_final[$temp->id] = $temp->name;
		}
		foreach ($subcategory as $temp) {
			$subcategory_final[$temp->id] = $temp->name;
		}
		foreach ($posttype as $temp) {
			$posttype_final[$temp->id] = $temp->name;
		}
		foreach ($user as $temp) {
			$user_final[$temp->id] = $temp->username;
		}
		return View('admin::updatePost',[
				'post' 		=> $post,
				'category' 	=> $category_final,
				'subcategory'=> $subcategory_final,
				'posttype'	=> $posttype_final,
				'user'		=> $user_final
		]);
	}

	public function doUpdatePost($id){
		$input = Input::all();
		$post = Post::find($id);
		$post->slug = $input['slug'];
		$post->user_id = $input['user_id'];
		$post->post_type_id = $input['posttype'];
		$post->subcategory_id = $input['subcategory'];
		$post->title = $input['title'];
		$post->message = $input['message'];
		$post->file_path = $input['filepath'];
		$post->video_url = $input['video_url'];
		$post->tags = $input['tags'];
		$post->allow_comment = $input['allowcomment'];
		$post->nsfw = $input['nsfw'];
		$post->reputation= $input['reputation'];
		$post->total_views = $input['totalview'];
		$post->audit_status = $input['audit_status'];
		$post->created_at = $input['createdat'];
		$post->updated_at = $input['updatedat'];
		$post->save();
		return Redirect::to('/admin/Posts');
	}

	public function insertPost(){
		$category = Category::all();
		$subcategory = SubCategory::all();
		$posttype = PostType::all();
		$user = User::all();
		foreach ($category as $temp) {
			$category_final[$temp->id] = $temp->name;
		}
		foreach ($subcategory as $temp) {
			$subcategory_final[$temp->id] = $temp->name;
		}
		foreach ($posttype as $temp) {
			$posttype_final[$temp->id] = $temp->name;
		}
		foreach ($user as $temp) {
			$user_final[$temp->id] = $temp->username;
		}
		return View('admin::insertPost',[
				'category' 	=> $category_final,
				'subcategory'=> $subcategory_final,
				'posttype'	=> $posttype_final,
				'user'		=> $user_final
		]);
	}

	public function doInsertPost(){
		$input = Input::all();
		$post = new Post;
		$post->slug = $input['slug'];
		$post->user_id = $input['user_id'];
		$post->post_type_id = $input['posttype'];
		$post->subcategory_id = $input['subcategory'];
		$post->title = $input['title'];
		$post->message = $input['message'];
		$post->file_path = $input['filepath'];
		$post->video_url = $input['video_url'];
		$post->tags = $input['tags'];
		$post->allow_comment = $input['allowcomment'];
		$post->nsfw = $input['nsfw'];
		$post->reputation= $input['reputation'];
		$post->total_views = $input['totalview'];
		$post->audit_status = $input['audit_status'];
		$post->created_at = $input['createdat'];
		$post->updated_at = $input['updatedat'];
		$post->save();
		return Redirect::to('/admin/Posts');
	}

	public function doDeletePost($id){
		$post = Post::find($id);
		$post->audit_status = "D";
		$post->save();
		return Redirect::to('/admin/Posts');
	}

}


?>