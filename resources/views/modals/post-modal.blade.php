<div class="modal fade" id="postModal" tabindex="-1" role="dialog" aria-labelledby="postModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">

                {!! Form::open(array('url' => 'post/upload', 'files' => true, 'method' => 'post')) !!}
                <div class="modal-top-wrap">
                    <div class="half">
                        <div class="append">Type</div>
                        <select name="posttype" id="myCombobox">
                            @foreach($posttypes as $type)
                            <option value="{!! $type->id !!}" {!! $type->id == 3 ? 'selected' : '' !!}>{!! $type->name; !!}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="half category-combobox">
                        <select name="category" id="categoryCombobox">
                            @foreach($subcategories as $sc)
                                <option class="red-background" value="{!! $sc->id !!}">{!! $sc->name !!}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <!-- IMAGE WINDOW --> 
                <div class="post-window window-image 1">
                    <div class="window-left">
                        {!! Form::text('title-image', '', array('id' => 'title-image', 'class' => 'form-control', 'placeholder' => 'Post title')) !!}
                        <div class="upload-wrap">
                                <span class="btn btn-success fileinput-button">
                                    <span class="purple">BROWSE IMAGE</span><br />
                                    <br /><br />
                                    <span>PNG, GIF or JPG accepted. <br /> Max Size is 5MB.</span>
                                    <!-- The file input field used as target for the file upload widget -->
                                    <input type="file" name="images[]" id="photoimg" rel="files-quote" multiple>
                            </span>
                            <div id="files-photos" class="files"></div>
                        </div>
                        <!--<input type="file" name="images[]" id="photoimg" rel="files-quote"/>-->
                    </div>
                    <aside>
                        <div class="outer">
                            <div class="inner">
                                <div class="custom-checkbox">
                                    <input type="checkbox" value="yes" name="check-comments-image" id="check-comments-image" />
                                    <label for="check-comments-image">ALLOW COMMENTS</label>
                                </div>
                                <div class="custom-checkbox">
                                    <input type="checkbox" value="yes" name="check-nsfw-image" id="check-nsfw-image" />
                                    <label for="check-nsfw-image">NSFW POST</label>
                                </div>
                            </div>
                        </div>
                        <div class="tags-wrap">
                            <h3>Tags:</h3>
                            <input name="tags-image" class="tags-selector" />
                        </div>
                    </aside>
                </div>
                <!-- VIDEO WINDOW -->
                <div class="post-window window-video 2">
                    <div class="window-left">
                        {!! Form::text('title-video', '', array('id' => 'title-video', 'class' => 'form-control', 'placeholder' => 'Post title')) !!}
                        {!! Form::text('video-url', '', array('id' => 'video-url', 'class' => 'form-control', 'placeholder' => 'Paste your link here (Youtube, Vimeo)')) !!}
                        <figure>
                            <div id="video-wrap" class="video-wrap">
                                <iframe width="560" height="315" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </figure>
                    </div>
                    <aside>
                        <div class="outer">
                            <div class="inner">
                                <div class="custom-checkbox">
                                    <input type="checkbox" value="yes" name="check-comments-video" id="check-comments-video" />
                                    <label for="check-comments-video">ALLOW COMMENTS</label>
                                </div>
                                <div class="custom-checkbox">
                                    <input type="checkbox" value="yes" name="check-nsfw-video" id="check-nsfw-video" />
                                    <label for="check-nsfw-video">NSFW POST</label>
                                </div>
                            </div>
                        </div>
                        <div class="tags-wrap">
                            <h3>Tags:</h3>
                            <input name="tags-video" class="tags-selector" />
                        </div>
                    </aside>
                </div>
                <!-- TEXT WINDOW -->
                <div class="post-window window-text 3">
                    <div class="window-left">
                        {!! Form::text('title-text', '', array('id' => 'title-text', 'class' => 'form-control', 'placeholder' => 'Post title')) !!}
                        {!! Form::textarea('message', null, array('id' => 'message', 'rows' => 3, 'class' => 'form-control quote-input', 'placeholder' => 'Your message')) !!}
                        <!-- <div class="upload-wrap">
                             <span class="btn btn-success fileinput-button">
                                 <span class="purple">OPTIONALLY BROWSE IMAGE</span><br />
                                 <span>NOT A MUST</span><br /><br />
                                 <span>PNG, GIF or JPG accepted. <br /> Max Size is 5MB.</span> -->
                                <!-- The file input field used as target for the file upload widget -->
                                <!-- <input class="fileupload-init" type="file" name="files[]" rel="files-qoute"> -->
                                <!-- <input type="file" name="images[]" id="photoimg" rel="files-quote" multiple>
                            </span>
                            <div id="files-qoute" class="files"></div>
                        </div> -->
                    </div>
                    <aside>
                        <div class="outer">
                            <div class="inner">
                                <div class="custom-checkbox">
                                    <input type="checkbox" value="yes" name="check-comments-text" id="check-comments-text" />
                                    <label for="check-comments-text">ALLOW COMMENTS</label>
                                </div>
                                <div class="custom-checkbox">
                                    <input type="checkbox" value="yes" name="check-nsfw-text" id="check-nsfw-text" />
                                    <label for="check-nsfw-text">NSFW POST</label>
                                </div>
                            </div>
                        </div>
                        <div class="tags-wrap">
                            <h3>Tags:</h3>
                            <input name="tags-text" class="tags-selector" />
                        </div>
                    </aside>
                </div>
                <div class="modal-bottom-menu">
                    <div class="full">
                        <button class="btn btn-link btn-block" onclick="return validatePost();">Upload</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>