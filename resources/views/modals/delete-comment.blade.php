<div class="modal fade" id="confirm-delete-comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        
            <div id="delete-header" class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-trash-o"></i> Delete Comment Confirmation</h4>
            </div>
        
            <div id="delete-body" class="modal-body">
                <p>You are about to delete your comment, this cannot be undone.</p>
                <p>Do you want to proceed?</p>
                <p class="debug-url" style="display: none;"></p>
            </div>
            
            <div  id="delete-footer" class="modal-footer">
                <button class="btn btn-default" data-dismiss='modal'>Cancel</button>
                <button id='btn-delete-comment' data-dismiss='modal' class="btn btn-danger btn-ok">Delete</button>
            </div>
        </div>
    </div>
</div>