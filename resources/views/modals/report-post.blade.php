<div class="modal fade" id="report-post" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        
            <div id="delete-header" class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-flag-o"></i> Report Post Confirmation</h4>
            </div>
        
            <div id="delete-body" class="modal-body">
                <fieldset>

                <!-- Select Basic -->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="reason">Why are you reporting this post?</label>
                  <div class="col-md-8">
                    <select id="report-post-reason" name="report-post-reason" class="form-control">
                      <option value="This post is spam.">This post is spam.</option>
                      <option value="This post is reposted.">This post is reposted.</option>
                      <option value="This post contains nudity and not marked as NSFW.">This post contains nudity and not marked as NSFW.</option>
                      <option value="This post is lacked of content quality.">This post is lacked of content quality.</option>
                      <option value="This post insulting specific race, religion, or ethnics.">This post insulting specific race, religion, or ethnics.</option>
                      <option value="My reason is not listed here.">My reason is not listed here.</option>
                    </select>
                  </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="description">Notes for us</label>
                  <div class="col-md-4">                     
                    <textarea class="form-control" id="report-post-description" name="report-post-description"></textarea>
                  </div>
                </div>

                </fieldset>
            </div>
            
            <div  id="delete-footer" class="modal-footer">
                <button id="cancel" name="cancel" class="btn btn-default"  data-dismiss='modal'>Cancel</button>
                <button type="submit" id="btn-report-post" name="btn-report-post" class="btn btn-danger"  data-dismiss='modal'>Report</button>
            </div>
        </div>
    </div>
</div>