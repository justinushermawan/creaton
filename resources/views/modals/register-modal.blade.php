<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-top-menu">
                    <div class="half">
                        <button class="btn btn-link btn-block active-ind" rel="register-window">Register new account</button>
                    </div>
                    <div class="half">
                        <button class="btn btn-link btn-block" rel="login-window">Login</button>
                    </div>
                </div>
                <div class="modal-window register-window">
                    {!! Form::open(array('url' => 'account/register', 'method' => 'post')) !!}
                    <div class="row">
                        <div class="col-sm-7 border-right">
                            <div class="form-group">
                                <label for="name">Username (www.creaton.com/your_username)</label>
                                {!! Form::text('username', '', array('id'=>'username', 'class'=>'form-control')) !!}
                            </div>
                            <!-- <div class="form-group">
                                <label for="name">Full Name</label>
                                {!! Form::text('name', '', array('id'=>'name', 'class'=>'form-control')) !!}
                            </div> -->
                            <div class="alert alert-danger" id="username-status" role="alert" style="display:none">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                {!! Form::text('email', '', array('id'=>'email', 'class'=>'form-control')) !!}
                            </div>
                            <div class="alert alert-danger" id="email-status" role="alert" style="display:none">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                {!! Form::password('password', array('id'=>'password', 'class'=>'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label for="rp-password">Re-enter Password</label>
                                {!! Form::password('rp-password', array('id'=>'rp-password', 'class'=>'form-control')) !!}
                            </div>
                            <div class="alert alert-danger" id="rp-password-status" role="alert" style="display:none">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="vertical-half">
                                <div class="custom-checkbox">
                                    <input type="checkbox" value="check1" name="check" id="check1" checked />
                                    <label for="check1">ARTS OF THE WEEK</label>
                                </div>
                                <div class="custom-checkbox">
                                    <input type="checkbox" value="check1" name="check" id="check2" />
                                    <label for="check2">NEWSLETTER</label>
                                </div>
                            </div>
                            <div class="vertical-half">
                                <div class="custom-checkbox-vertical custom-checkbox">
                                    <p>I promise only to have fun here and I fully agree with the</p>
                                    <input type="checkbox" value="check1" name="check" id="agreeTerms" />
                                    <label for="agreeTerms">Terms of Agreement.</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input id="register-button" type="submit" value="NICE, REGISTER ME!" class="btn btn-primary btn-block custom-button" />
                        <a id="error-button" class="btn btn-block btn-info custom-button" href="#">WHY DON’T YOU AGREE WITH OUR ToA?</a>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-window login-window">
                    <div class="half-horizontal">
                        <div class="input-group">
                            <span class="input-group-addon">Username:</span>
                            <input type="text" class="form-control" value="loginteothemes">
                        </div>
                    </div>
                    <div class="half-horizontal">
                        <div class="input-group">
                            <span class="input-group-addon">Password:</span>
                            <input type="password" class="form-control" value="password">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-block btn-info custom-button" href="#">CAN'T LOGIN YET</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>