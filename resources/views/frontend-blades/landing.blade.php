<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Creaton, Inc.">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Creaton - Your Creation Gallery and Community</title>
	<!-- CSS -->
	<link href="{!! URL::asset('css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('css/bootstrap-social.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/font-awesome.min.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/prettyPhoto.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/animate.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/main.css') !!}" rel="stylesheet">
</head>
<body>

	<!-- Creaton Header -->
	<header class="navbar navbar-inverse navbar-fixed-top wet-asphalt" role="banner">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Main Toggle</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ URL::to('/') }}" rel="home" title="Creaton Logo">
                    <img style="max-width:180px; margin-top: -7px;" src="{{ URL::asset('images/logo.png') }}">            
                </a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
                    <li>
                        <button class="btn btn-primary" onclick="location.href='r.php'">Become a Creator</button>
                    </li>
                    <li class="dropdown" id="menuLogin">
                      <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="navLogin">Sign In <i class="icon-angle-down"></i></a>
                      <div class="dropdown-menu" style="padding:17px;">
                        {!! Form::open(array('url' => 'account/login')) !!} 
                          @if(Session::has('error'))
                            <div class="alert-box success">
                                <h2>{!! Session::get('error') !!}</h2>
                            </div>
                          @endif
                          <div class="form-group">
                            {!! Form::text('username', '', array('id'=>'', 'class'=>'form-control', 'placeholder'=>'Phone, email or username')) !!}
                            <p class="errors">{!! $errors->first('username') !!}</p>
                          </div>
                          <div class="form-group">
                            {!! Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) !!}
                            <p class="errors">{!! $errors->first('password') !!}</p>
                          </div>
                          <div class="form-group">
                            <div class="checkbox">
                              <label>
                                <input id="login-remember" type="checkbox" name="remember" value="1" checked="checked"> Remember me
                              </label>
                            </div>
                          </div>
                          <div class="form-group">
                            {!! Form::submit('Sign in', array('class'=>'btn btn-default btn-lg')) !!}
                          </div>
                        {!! Form::close() !!}
                      </div>
                    </li>
                    <!-- 
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Language: English (US) <i class="icon-angle-down"></i></a>
						<ul class="dropdown-menu">
                            <li><a href="#">English (US)</a></li>
							<li><a href="#">Bahasa Indonesia</a></li>
							<li><a href="#">Bahasa Melayu</a></li>
							<li><a href="#">Deutsch</a></li>
							<li><a href="#">English (UK)</a></li>
						</ul>
					</li>
                    -->
				</ul>
			</div>
		</div>
	</header> <!-- End of Header -->

	<!-- Carousel -->
	<section id="main-slider" class="no-margin">
        <div class="carousel slide wet-asphalt">
            <ol class="carousel-indicators">
                <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                <li data-target="#main-slider" data-slide-to="1"></li>
                <li data-target="#main-slider" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active" style="background-image: url({!! URL::asset('images/slider/bg1.jpg') !!})">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content centered">
                                    <h2 class="animation animated-item-1">Welcome to Creaton.</h2>
                                    <p class="animation animated-item-2">A great place to show-off your talent.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
                <div class="item" style="background-image: url({!! URL::asset('images/slider/bg2.jpg') !!})">">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content center centered">
                                    <h2 class="boxed animation animated-item-1">Welcome to Creaton.</h2>
                                    <p class="boxed animation animated-item-2">Connect with all creatives in the world — and other fascinating people. Get in-the-moment updates on the things that interest you. And watch events unfold, in real time, from every angle.</p>
                                    <br>
                                    <a class="btn btn-md animation animated-item-3" href="#">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!--/.item--><!--/.item-->
                <div class="item" style="background-image: url({!! URL::asset('images/slider/bg3.jpg') !!})">">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content center centered">
                                    <h2 class="boxed animation animated-item-1">Welcome to Creaton.</h2>
                                    <p class="boxed animation animated-item-2">Let's join with the Top Creators in Indonesia.</p>
                                    <br>
                                    <a class="btn btn-md animation animated-item-3" href="{{ URL::to('r.php') }}">Join Today</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!--/.item-->
            </div> <!--/.carousel-inner-->
        </div> <!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="icon-angle-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="icon-angle-right"></i>
        </a>
    </section> <!-- End of Carousel -->

    <!-- Footer -->
    <div class="navbar navbar-default">
        <div class="container">
        <p class="navbar-text pull-left">Creaton &copy; 2015</p>
      
        <a href="http://youtu.be/zJahlKPCL9g" class="navbar-btn btn-info btn pull-right">
        <span class="glyphicon glyphicon-star"></span>  Follow Us on Twitter</a>
    </div>
    <!-- End of Footer -->

	<!-- JS -->
	<script type="text/javascript" src="{!! URL::asset('js/jquery.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/bootstrap.min.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/main.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/jquery.prettyPhoto.js') !!}"></script>
</body>
</html>