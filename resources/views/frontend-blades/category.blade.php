@extends('frontend-blades.master')

@section('pre-content')
<div class="col-sm-9 right-content">
    <div class="row">
        <div class="category-wrap">
            <div class="col-sm-4 border-right">
                <div class="outer">
                    <div class="inner">
                        <h2>{!! $category->name !!}</h2>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 border-right">
                <div class="outer">
                    <div class="inner">
                        <div class="total-posts">{!! $subCount !!}</div>
                        <div class="total-posts-description">Subcategories</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-sm-9">
@stop

@section('content')
    @if(count($childSubs))
    <ul>
        @foreach($childSubs as $sub)
            <li><a href="{!! URL::to('/sc/'.$sub->slug) !!}"> {!! $sub->name !!} ({!! $sub->postCount() !!})</a></li>
        @endforeach
    </ul>
    @endif

    @include('modals.delete-post')
    @include('modals.report-post')
@stop