@extends('frontend-blades.master')

@section('pre-content')
<div class="col-sm-9 right-content">
    <div class="row">
        <div class="category-wrap">
            <div class="col-sm-4 border-right">
                <div class="outer">
                    <div class="inner">
                        <h2>{!! $sub->name !!}</h2>
                        <h3>Subcategory of <a href="{!! URL::to('/c/'.$sub->category->slug) !!}">{!! $sub->category->name !!}</a></h3>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 border-right">
                <div class="outer">
                    <div class="inner">
                        <div class="total-posts">{!! $postCount !!}</div>
                        <div class="total-posts-description">Total Posts</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-sm-9">
@stop

@section('content')
    @if(count($posts))
    <h1>creations posted under {{ $sub->name }}</h1>
    <div class="separator"></div>
    @else
    <h1>There is no creations posted under {{ $sub->name }}</h1>
    @endif

    @foreach($posts as $post)

        @include('partials._post', ['post' => $post])

    @endforeach

    @include('modals.delete-post')
    @include('modals.report-post')
@stop