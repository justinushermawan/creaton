<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Creaton, Inc.">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Forgot Password | Can't Log In | Creaton</title>
	<!-- CSS -->
	<link href="{!! URL::asset('css/bootstrap.min.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/font-awesome.min.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/prettyPhoto.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/animate.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/main.css') !!}" rel="stylesheet">
</head>
<body>

	<!-- Creaton Header -->
	<header class="navbar navbar-inverse navbar-fixed-top wet-asphalt" role="banner">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Main Toggle</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ URL::to('/') }}">Creaton</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Language: English (US) <i class="icon-angle-down"></i></a>
						<ul class="dropdown-menu">
                            <li><a href="#">English (US)</a></li>
							<li><a href="#">Bahasa Indonesia</a></li>
							<li><a href="#">Bahasa Melayu</a></li>
							<li><a href="#">Deutsch</a></li>
							<li><a href="#">English (UK)</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</header> <!-- End of Header -->

	<!-- Forgot Password Panel -->
	<div class="container" style="margin-top:50px">
        <div class="row">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="text-center">
                              <h3 class="text-center">Forgot Password?</h3>
                              <p>If you have forgotten your password - reset it here.</p>
                                <div class="panel-body">
                                  
                                    <form action="{{ URL::route('forgot-password-post') }}" method="post">
                                        <fieldset>
                                          <div class="form-group">
                                            <div class="input-group">
                                              <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                              <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                              <!--EMAIL ADDRESS-->
                                              <input id="email" name="email" placeholder="Email Address" class="form-control" type="email" oninvalid="setCustomValidity('Please enter a valid email address!')" onchange="try{setCustomValidity('')}catch(e){}" required="true">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <input class="btn btn-lg btn-primary btn-block" value="Send My Password" type="submit">
                                          </div>
                                        </fieldset>
                                    </form>
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Forgot Password Panel -->

	<!-- JS -->
	<script type="text/javascript" src="{!! URL::asset('js/jquery.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/bootstrap.min.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/main.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/jquery.prettyPhoto.js') !!}"></script>
</body>
</html>