<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Creaton - A Place for Artist</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <link rel="icon" type="image/png" href="{!! URL::asset('ct-themes/favicon.png') !!}">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{!! URL::asset('css/grid-masonry.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('css/font-awesome/css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/slider.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/main.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/jquery.selectBoxIt.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/jquery.mCustomScrollbar.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/fileupload/css/jquery.fileupload.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/fileupload/css/jquery.fileupload-ui.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/jquery.tagsinput.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/style.css') !!}">

    <script src="{!! URL::asset('ct-themes/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery-1.10.1.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.oembed.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('js/masonry.pkgd.min.js') !!}"></script>
    <!-- JS by Creaton -->
    <script type="text/javascript" src="{!! URL::asset('js/creaton.js') !!}"></script>


    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=225987124123858";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</head>
<body>
@include('partials._header')
<section id="main" role="main">
    <div class="container-search">
        <div class="row">
            <div>
                <div class="col-sm-12">
                    <div class="outer">
                        <div class="inner">
                            <h1>SEARCH ({!! $searchCount <= 1 ? $searchCount . ' result' : $searchCount . ' results' !!})</h1>
                            <hr/>
                            {!! Form::open(array('url' => 'advanced_search', 'method' => 'POST', 'class' => 'form-inline')) !!}
                                <div class="input-group col-sm-9">
                                    <div class="input-group-addon"> 
                                        <label for="searchby" class="control-label">Search for&nbsp;</label>
                                        {!! Form::select('searchby', array(0 => 'posts', 1 => 'creators'), isset($searchBy) ? $searchBy : 0, array('id' => 'searchby', 'style' => 'border: 0; border-bottom: 1px solid #a3a3a3;')) !!}
                                        <label for="orderby" class="control-label">&nbsp;order by&nbsp;</label>
                                        {!! Form::select('orderby', array(0 => 'time', 1 => 'reputation'), isset($orderBy) ? $orderBy : 0, array('id' => 'orderby', 'style' => 'border: 0; border-bottom: 1px solid #a3a3a3;')) !!}
                                        <label for="txt-search" class="control-label">&nbsp;with keyword&nbsp;</label>
                                    </div>
                                    <input type="text" class="form-control" id="key" name="key" style="height: 50px;" value="{!! $key !!}" placeholder="insert keyword.."/> 
                                    <div class="input-group-addon">
                                        {!! Form::select('ordertype', array(0 => 'ascending', 1 => 'descending'), isset($orderType) ? $orderType : 0, array('id' => 'ordertype', 'style' => 'border: 0; border-bottom: 1px solid #a3a3a3;')) !!}
                                    </div>
                                    <div class="input-group-addon">
                                        <button type="submit" id="btn-search" class="btn btn-link"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="wrapper">
                    <!-- <div class="m-grid-item"> -->
                    @if(count($posts))
                    @foreach($posts as $post)
                        <div class="pin">
                        @include('partials._post', ["post" => $post])
                        </div>
                    @endforeach
                    @elseif(count($users))
                    @foreach($users as $user)

                    <article class="main-user" id="{!! $user->id !!}">
                        <div class="profile-wrap" style="margin-left: 0; background-image: url('{{ URL::asset('ct-themes/img/user-cover.jpg') }}');">
                        <div class="col-sm-9">
                            <div class="outer">
                                <div class="inner">
                                    <figure>
                                        <img src="{{ URL::asset('userdata/' . $user->username . '/user.png') }}" alt=""/>
                                    </figure>
                                    <div class="text">
                                        <div class="outer">
                                            <div class="inner">
                                                <a href="{!! '' . $user->username !!}" class="user-name">{!! $user->fullname !!}</a>
                                                <div class="counters">I'm a {!! Str::lower($user->title) !!}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="outer">
                                <div class="inner">
                                    <div class="counter-item">
                                        <span class="number">{!! $user->reputation !!}</span><br/>
                                        <span class="exp">reps</span>
                                    </div>
                                    <div class="counter-item">
                                        <span class="number">{!! $user->postsCount() !!}</span><br/>
                                        <span class="exp">posts</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </article>

                    @endforeach
                    @else
                    <h3>Sorry, there is nothing for keyword "{!! $key !!}". Try another keyword :)</h3>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('partials._footer')
<a href="#" class="scroll-top">&nbsp;</a>
@include('modals.register-modal')
@include('modals.post-modal')
@include('modals.delete-post')
<div id="scripts">
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery-ui.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.flexslider-min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.selectBoxIt.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.mCustomScrollbar.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.mousewheel.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/vendor/jquery.ui.widget.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.tagsinput.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.iframe-transport.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-process.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-image.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-audio.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-video.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-validate.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/main.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/retina-1.1.0.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/jquery.lazyload.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/pwstrength.js') !!}"></script>
</div>
</body>
</html>