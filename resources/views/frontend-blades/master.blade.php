<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Creaton - A Place for Artist</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <link rel="icon" type="image/png" href="{!! URL::asset('ct-themes/favicon.png') !!}">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{!! URL::asset('css/font-awesome/css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/slider.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/main.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/jquery.selectBoxIt.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/jquery.mCustomScrollbar.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/fileupload/css/jquery.fileupload.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/fileupload/css/jquery.fileupload-ui.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/jquery.tagsinput.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/style.css') !!}">

    <script src="{!! URL::asset('ct-themes/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery-1.10.1.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.oembed.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('js/masonry.pkgd.min.js') !!}"></script>
    <!-- JS by Creaton -->
    <script type="text/javascript" src="{!! URL::asset('js/creaton.js') !!}"></script>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=225987124123858";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</head>
<body>
@include('partials._header')
<section id="main" role="main">
    <div class="container">
        <div class="row">
            @yield('pre-content')
            <div class="main-wrap">
                @yield('content')
            </div>
        </div>
            @include('partials._widgets')
    </div>
            </div>
            <aside class="col-sm-3 left-aside">
                <div class="sidebar-menu">
                    <div class="sidebar-content sidebar-content-category">
                        @include('partials._categories')
                    </div>
                    <div class="sidebar-content sidebar-content-hot">
                        <div class="side-article">
                                <figure>
                                    <a href="post.html">
                                        <img src="{!! URL::asset('ct-themes/img/left1.jpg') !!}" alt=""/>
                                    </a>
                                </figure>
                                <h2><a href="post.html">People Cherising New Year's Eve
</a></h2>
                                <div class="counters-line">
                                    <div class="pull-left">
                                        <div class="date"><i class="icon-date"></i> 23.04</div>
                                        <div class="user"><i class="icon-user"></i> <a href="profile.html">johnjsu</a></div>
                                        <div class="comments"><i class="icon-comments"></i> <a href="post.html">320</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-article">
                                <figure>
                                    <a href="post.html">
                                        <img src="{!! URL::asset('ct-themes/img/left2.jpg') !!}" alt=""/>
                                    </a>
                                </figure>
                                <h2><a href="post.html">How to get killed and revive in 3 days!</a></h2>
                                <div class="counters-line">
                                    <div class="pull-left">
                                        <div class="date"><i class="icon-date"></i> 23.04</div>
                                        <div class="user"><i class="icon-user"></i> <a href="profile.html">johnjsu</a></div>
                                        <div class="comments"><i class="icon-comments"></i> <a href="post.html">320</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-article">
                                <figure>
                                    <a href="post-2.html">
                                        <img src="{!! URL::asset('ct-themes/img/left3.jpg') !!}" alt=""/>
                                    </a>
                                </figure>
                                <h2><a href="post.html">How Photoshop Really Works</a></h2>
                                <div class="counters-line">
                                    <div class="pull-left">
                                        <div class="date"><i class="icon-date"></i> 23.04</div>
                                        <div class="user"><i class="icon-user"></i> <a href="profile.html">johnjsu</a></div>
                                        <div class="comments"><i class="icon-comments"></i> <a href="post.html">320</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-article">
                                <figure>
                                    <a href="post.html">
                                        <img src="{!! URL::asset('ct-themes/img/left4.jpg') !!}" alt=""/>
                                    </a>
                                </figure>
                                <h2><a href="post.html">People Cherising New Year's Eve
</a></h2>
                                <div class="counters-line">
                                    <div class="pull-left">
                                        <div class="date"><i class="icon-date"></i> 23.04</div>
                                        <div class="user"><i class="icon-user"></i> <a href="profile.html">johnjsu</a></div>
                                        <div class="comments"><i class="icon-comments"></i> <a href="post.html">320</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-article">
                                <figure>
                                    <a href="post.html">
                                        <img src="{!! URL::asset('ct-themes/img/left5.jpg') !!}" alt=""/>
                                    </a>
                                </figure>
                                <h2><a href="post.html">How to get killed and revive in 3 days!</a></h2>
                                <div class="counters-line">
                                    <div class="pull-left">
                                        <div class="date"><i class="icon-date"></i> 23.04</div>
                                        <div class="user"><i class="icon-user"></i> <a href="profile.html">johnjsu</a></div>
                                        <div class="comments"><i class="icon-comments"></i> <a href="post.html">320</a></div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="sidebar-content sidebar-content-bookmarks">
                        <div class="side-article">
                                <figure>
                                    <a href="post.html">
                                        <img src="{!! URL::asset('ct-themes/img/left1.jpg') !!}" alt=""/>
                                    </a>
                                </figure>
                                <h2><a href="post.html">People Cherising New Year's Eve
</a></h2>
                                <div class="counters-line">
                                    <div class="pull-left">
                                        <div class="date"><i class="icon-date"></i> 23.04</div>
                                        <div class="user"><i class="icon-user"></i> <a href="profile.html">johnjsu</a></div>
                                        <div class="comments"><i class="icon-comments"></i> <a href="post.html">320</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-article">
                                <figure>
                                    <a href="post.html">
                                        <img src="{!! URL::asset('ct-themes/img/left2.jpg') !!}" alt=""/>
                                    </a>
                                </figure>
                                <h2><a href="post.html">How to get killed and revive in 3 days!</a></h2>
                                <div class="counters-line">
                                    <div class="pull-left">
                                        <div class="date"><i class="icon-date"></i> 23.04</div>
                                        <div class="user"><i class="icon-user"></i> <a href="profile.html">johnjsu</a></div>
                                        <div class="comments"><i class="icon-comments"></i> <a href="post.html">320</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-article">
                                <figure>
                                    <a href="post-2.html">
                                        <img src="{!! URL::asset('ct-themes/img/left3.jpg') !!}" alt=""/>
                                    </a>
                                </figure>
                                <h2><a href="post.html">How Photoshop Really Works</a></h2>
                                <div class="counters-line">
                                    <div class="pull-left">
                                        <div class="date"><i class="icon-date"></i> 23.04</div>
                                        <div class="user"><i class="icon-user"></i> <a href="profile.html">johnjsu</a></div>
                                        <div class="comments"><i class="icon-comments"></i> <a href="post.html">320</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-article">
                                <figure>
                                    <a href="post.html">
                                        <img src="{!! URL::asset('ct-themes/img/left4.jpg') !!}" alt=""/>
                                    </a>
                                </figure>
                                <h2><a href="post.html">People Cherising New Year's Eve
</a></h2>
                                <div class="counters-line">
                                    <div class="pull-left">
                                        <div class="date"><i class="icon-date"></i> 23.04</div>
                                        <div class="user"><i class="icon-user"></i> <a href="profile.html">johnjsu</a></div>
                                        <div class="comments"><i class="icon-comments"></i> <a href="post.html">320</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="side-article">
                                <figure>
                                    <a href="post.html">
                                        <img src="{!! URL::asset('ct-themes/img/left5.jpg') !!}" alt=""/>
                                    </a>
                                </figure>
                                <h2><a href="post.html">How to get killed and revive in 3 days!</a></h2>
                                <div class="counters-line">
                                    <div class="pull-left">
                                        <div class="date"><i class="icon-date"></i> 23.04</div>
                                        <div class="user"><i class="icon-user"></i> <a href="profile.html">johnjsu</a></div>
                                        <div class="comments"><i class="icon-comments"></i> <a href="post.html">320</a></div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</section>
@include('partials._footer')
<a href="#" class="scroll-top">&nbsp;</a>
@include('modals.register-modal')
@include('modals.post-modal')
<div id="scripts">
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery-ui.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.flexslider-min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.selectBoxIt.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.mCustomScrollbar.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.mousewheel.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/vendor/jquery.ui.widget.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.tagsinput.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.iframe-transport.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-process.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-image.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-audio.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-video.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-validate.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/main.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/retina-1.1.0.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/jquery.lazyload.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/pwstrength.js') !!}"></script>
</div>
</body>
</html>