@extends('frontend-blades.master')

@section('pre-content')
    <div class="col-sm-9 right-content profile-bg">
    <div class="row">
        <div class="profile-wrap" style="background-image: url('{{ URL::asset('ct-themes/img/user-cover.jpg') }}');">
            <div class="col-sm-9">
                <div class="outer">
                    <div class="inner">
                        <figure>
                            <img src="{{ URL::asset('ct-themes/img/user.png') }}" alt=""/>
                        </figure>
                        <div class="text">
                            <div class="outer">
                                <div class="inner">
                                    <a href="#" class="user-name">{{ $fullname }}</a>
                                    <div class="counters">I'm a {{ Str::lower($title) }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="outer">
                    <div class="inner">
                        <div class="counter-item">
                            <span class="number">{{ $reps }}</span><br/>
                            <span class="exp">reps</span>
                        </div>
                        <div class="counter-item">
                            <span class="number">{{ $totalposts }}</span><br/>
                            <span class="exp">posts</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-sm-9">
@stop

@section('content')
    <h1>posts by {{ $username }}</h1>
    <div class="separator"></div>

    @foreach($posts as $post)

        @include('partials._post', ['post' => $post])

    @endforeach
    @include('modals.delete-post')
    @include('modals.report-post')
@stop