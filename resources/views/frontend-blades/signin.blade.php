<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Creaton, Inc.">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Log into Creaton | Creaton</title>
	<!-- CSS -->
	<link href="{!! URL::asset('css/bootstrap.min.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/font-awesome.min.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/prettyPhoto.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/animate.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/main.css') !!}" rel="stylesheet">
</head>
<body>

	<!-- Creaton Header -->
	<header class="navbar navbar-inverse navbar-fixed-top wet-asphalt" role="banner">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Main Toggle</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ URL::to('/') }}">
                    <img style="max-width:180px; margin-top: -7px;" src="{{ URL::asset('images/logo.png') }}"> 
                </a>
			</div>
            <!-- 
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Language: English (US) <i class="icon-angle-down"></i></a>
						<ul class="dropdown-menu">
                            <li><a href="#">English (US)</a></li>
							<li><a href="#">Bahasa Indonesia</a></li>
							<li><a href="#">Bahasa Melayu</a></li>
							<li><a href="#">Deutsch</a></li>
							<li><a href="#">English (UK)</a></li>
						</ul>
					</li>
				</ul>
			</div>
            -->
		</div>
	</header> <!-- End of Header -->

	<!-- Sign In Panel -->
	<div class="container">    
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
            	{!! Form::open(array('url' => 'account/login')) !!}
                <div class="panel-heading">
                    <div class="panel-title">Sign In</div>
                    <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="{{ URL::to('/login/recover') }}">Forgot password?</a></div>
                </div>     

                <div style="padding-top:30px" class="panel-body" >
                    <div id="login-alert" class="alert alert-success col-sm-12">
                        @if (Session::has('flash_message'))
                            {{ Session::get('flash_message') }}
                        @endif
                    </div>          
                    <form id="loginform" class="form-horizontal" role="form">       
                        	<div style="margin-bottom: 25px" class="input-group">
                            	<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            	{!! Form::text('username', '', array('id'=>'', 'class'=>'form-control', 'placeholder'=>'Phone, email or username')) !!}
                        	</div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                {!! Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) !!}
                            </div>
                            
                            <div class="input-group">
                                <div class="checkbox">
                                    <label>
                                        <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                    </label>
                                </div>
                            </div>

                            <div style="margin-top:10px" class="form-group">
                                <!-- Button -->

                                <div class="col-sm-12 controls">
                                    {!! Form::submit('Sign in', array('class'=>'btn btn-success')) !!}
                                    <a id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 control">
                                    <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                        Don't have an account! 
                                    <a href="r.php">
                                        Sign Up Here
                                    </a>
                                    </div>
                                </div>
                            </div>    
                        </form>
                    </div>                     
                </div> 
            {!! Form::close() !!} 
        </div>
    </div>
	<!-- End of Sign In Panel -->

	<!-- JS -->
	<script type="text/javascript" src="{!! URL::asset('js/jquery.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/bootstrap.min.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/main.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/jquery.prettyPhoto.js') !!}"></script>
</body>
</html>