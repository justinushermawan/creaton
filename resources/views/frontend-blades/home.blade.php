@extends('frontend-blades.master')

@section('pre-content')
    <div class="col-sm-9 right-content">
        <div class="row">
            <div class="col-sm-9">
@stop

@section('content')

    
    @if($posts->count() > 0)

    @foreach($posts as $post)

        @include('partials._post', ['post', $post])

    @endforeach

    <button id="button-more" class="btn btn-default" style="display:block; width:100%;" data-value="{{ count($posts) }}" type="button" onclick="lazyload.load()">
        Load More
    </button>

    <div id="loading-div" style="display:none">
        loading more items
    </div>

    @else

    <p>There is no post uploaded</p>

    @endif

    @include('modals.delete-post')
    @include('modals.report-post')
@stop