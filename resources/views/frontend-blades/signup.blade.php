<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="Creaton, Inc.">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Sign Up for Creaton | Creaton</title>
	<!-- CSS -->
	<link href="{!! URL::asset('css/bootstrap.min.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/font-awesome.min.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/prettyPhoto.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/animate.css') !!}" rel="stylesheet">
	<link href="{!! URL::asset('css/main.css') !!}" rel="stylesheet">
</head>
<body>

	<!-- Creaton Header -->
	<header class="navbar navbar-inverse navbar-fixed-top wet-asphalt" role="banner">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Main Toggle</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ URL::to('/') }}">
                    <img style="max-width:180px; margin-top: -7px;" src="{{ URL::asset('images/logo.png') }}">
                </a>
			</div>
            <!-- 
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Language: English (US) <i class="icon-angle-down"></i></a>
						<ul class="dropdown-menu">
                            <li><a href="#">English (US)</a></li>
							<li><a href="#">Bahasa Indonesia</a></li>
							<li><a href="#">Bahasa Melayu</a></li>
							<li><a href="#">Deutsch</a></li>
							<li><a href="#">English (UK)</a></li>
						</ul>
					</li>
				</ul>
			</div>
            -->
		</div>
	</header> <!-- End of Header -->

	<!-- Sign Up Panel -->
	<div class="container">
        <div id="signupbox" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">Sign Up</div>
                    <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="login.php">Sign In</a></div>
                </div>  
                <div class="panel-body" >
                    {!! Form::open(array('url' => 'account/register', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post')) !!}
                        <div id="signupalert" style="display:none" class="alert alert-danger">
                            <p>Error:</p>
                            <span></span>
                        </div>
                        
                        <div class="form-group">
                            <label for="name" class="col-md-3 control-label">Username</label>
                            <div class="col-md-9">
                                {!! Form::text('username', '', array('id'=>'username', 'required' => 'true', 'class'=>'form-control', 'placeholder' => 'www.creaton.id/your_username')) !!}
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label for="email" class="col-md-3 control-label">Email</label>
                            <div class="col-md-9">
                                {!! Form::email('email', '', array('id'=>'email', 'required' => 'true', 'class'=>'form-control', 'placeholder' => 'example@example.com')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-3 control-label">Password</label>
                            <div class="col-md-9">
                                {!! Form::password('password', array('id'=>'password', 'required' => 'true', 'class'=>'form-control', 'placeholder' => 'Password')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="rp-password" class="col-md-3 control-label">Re-enter Password</label>
                            <div class="col-md-9">
                                {!! Form::password('rp-password', array('id'=>'rp-password', 'required' => 'true', 'class'=>'form-control', 'placeholder' => 'Re-type Password')) !!}
                            </div>
                        </div>
                        
                        <div class="alert alert-danger" id="rp-password-status" role="alert" style="display:none">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                        </div>
                            
                        <div class="form-group">
                            <div class="col-md-9">
                                <div class="captcha">
                                  <div id="recaptcha_image"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9">
                                <div class="recaptcha_only_if_image">Enter the words above</div>
                                <div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
                                <div class="input-group">
                                    <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" class="form-control input-lg" />
                                    <a class="btn btn-default input-group-addon" href="javascript:Recaptcha.reload()"><span class="glyphicon glyphicon-refresh"></span></a>
                                    <a class="btn btn-default input-group-addon recaptcha_only_if_image" href="javascript:Recaptcha.switch_type('audio')"><span class="glyphicon glyphicon-volume-up"></span></a>
                                    <a class="btn btn-default input-group-addon recaptcha_only_if_audio" href="javascript:Recaptcha.switch_type('image')"><span class="glyphicon glyphicon-picture"></span></a>
                                    <a class="btn btn-default input-group-addon" href="javascript:Recaptcha.showhelp()"><span class="glyphicon glyphicon-info-sign"></span></a>
                                </div>
                                {{ \App\Captcha\CaptchaGenerator::showCaptcha() }}
                            </div>
                        </div>

                        <div class="form-group">
                            <!-- Button -->                                        
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit('Sign Up', array('class'=>'btn btn-info')) !!}
                                <span style="margin-left:8px;">or</span>  
                            </div>
                        </div>
                        
                        <div style="border-top: 1px solid #999; padding-top:20px"  class="form-group">                            
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-fbsignup" type="button" class="btn btn-primary"><i class="icon-facebook"></i>   Sign Up with Facebook</button>
                            </div>                                                                          
                        </div>   
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
	<!-- End of Sign Up Panel -->

	<!-- JS -->
	<script type="text/javascript" src="{!! URL::asset('js/jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('js/creaton.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/bootstrap.min.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/main.js') !!}"></script>
	<script type="text/javascript" src="{!! URL::asset('js/jquery.prettyPhoto.js') !!}"></script>
</body>
</html>