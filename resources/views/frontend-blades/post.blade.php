@extends('frontend-blades.master')

@section('pre-content')
    <div class="col-sm-9 right-content">
        <div class="row">
            <div class="col-sm-9">
@stop

@section('content')
	<article class="main-post post-page">
	    <div class="article-top">
	        <h1><a href="">{!! $post->title !!}</a></h1>
	        <hr />
	        <div class="counters-line">
	            <div class="pull-left">
	                <div class="date"><i class="fa fa-calendar"></i>{!! $post->created_at->format('d/m/Y') !!}</div>
                    <div class="user"><i class="fa fa-user"></i> <a href="{!! URL::to('/'.$post->user->username) !!}">{!! $post->user->username !!}</a></div>
                    <div class="views"><i class="fa fa-eye"></i>{!! $post->total_views !!} views</div>
                    @if($post->allow_comment)
                    <div class="comments">
                        <i class="fa fa-comment"></i> 
                        <a href="{!!'post/' . $post->id!!}">{!! $post->commentCount() !!}</a> comments
                    </div>
                    @endif
                    <div class="like"><i class="fa fa-thumbs-o-up"></i><span id="upvote{!! $post->id !!}">{!! $post->reputation !!}</span> reps</div>
	            </div>
                <div class="pull-right">
                    @if($post->nsfw)
                    <div class="corner-tag red">NSFW</div>
                    @endif
                </div>
	        </div>
	        <div class="buttons-bar">
	            <div class="buttons">
	                <a href="" class="report has-tooltip" data-title="Report post" data-href="{!! $post->id !!}" data-toggle="modal" data-target="#report-post" style="{!! $post->isReportedBy(Auth::user()->id)? 'display:none' : 'display: initial' !!}">
                        <i class="fa fa-flag-o"></i>
                    </a>
                    <a class="reported has-tooltip" data-title="Post reported" style="{!! $post->isReportedBy(Auth::user()->id) == 0? 'display:none' : 'display: initial' !!}">
                        <i class="fa fa-flag"></i>
                    </a>
	            </div>
                <div class="social-icons">
                    {{-- Show upvoted button if user has voted --}}
                    <a href="" class="upvoted has-tooltip" data-title="Upvoted" style="{!! $post->isVoted(Auth::user()->id) ? 'display:initial' : 'display:none' !!}"><i user-id='{!! Auth::user()->id !!}' post-id='{!! $post->id !!}' class="fa fa-thumbs-up"></i></a>
                    {{-- Show upvote button if user has not voted yet --}}
                    <a href="" class="upvote has-tooltip" data-title="Upvote" style="{!! $post->isVoted(Auth::user()->id) == 0? 'display:initial' : 'display:none' !!}"><i user-id='{!! Auth::user()->id !!}' post-id='{!! $post->id !!}' class="fa fa-thumbs-o-up"></i></a>

                    <a href="javascript:void(0)" class='facebook has-tooltip' data-title="Share on Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="javascript:void(0)" class='twitter has-tooltip' data-title="Share on Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="javascript:void(0)" class='googleplus has-tooltip' data-title="Share on Google +"><i class="fa fa-google-plus"></i></a>
                    {{-- Show delete button if the post is posted by this user --}}
                    @if($post->user->id == Auth::user()->id)
                    <a href="#" class="other has-tooltip" data-title="Delete post" data-href="{!! $post->id !!}-detail" data-toggle="modal" data-target="#confirm-delete-post"><i class="fa fa-trash-o"></i></a>
                    @endif
                </div>
	        </div>
	    </div>
	    <div class="article-content">
	        
            {{-- IMAGE POST --}}
            @if($post->post_type_id == 1)
            <figure>
                <div class="corner-tag  {!! $post->subcategory->category->color !!}"><a href="{!! URL::to('sc/'.$post->subcategory->slug) !!}">{!! $post->subcategory->name !!}</a></div>
                <img class="lazy" data-original="{!! URL::asset('userdata/' . $post->user->username . '/post/' . $post->file_path) !!}" alt=""/>
            </figure>

            {{-- VIDEO POST --}}
            @elseif($post->post_type_id == 2)
            <figure>
                <div class="corner-tag  {!! $post->subcategory->category->color !!}"><a href="{!! URL::to('sc/'.$post->subcategory->slug) !!}">{!! $post->subcategory->name !!}</a></div>
                <div class="{!! $post->id !!} video-post">
                    <input type="hidden" id="{!! $post->id !!}" class="post_video_url" value="{!! $post->video_url !!}">
                    <iframe width="560" height="315" frameborder="0" allowfullscreen></iframe>
                    <script type="text/javascript">
                        var post_id = $('.post_video_url').attr('id');
                        var post_number = post_id + '';
                        var url = $('#' + post_number + '').attr('value');
                        loadVideoPost(url, post_id);
                    </script>
                </div>
            </figure>

            {{-- TEXT POST --}}
            @elseif($post->post_type_id == 3)
            <div class="quote-wrap">
                <div class="corner-tag  {!! $post->subcategory->category->color !!}"><a href="{!! URL::to('sc/'.$post->subcategory->slug) !!}">{!! $post->subcategory->name !!}</a></div>
                <h2>Message</h2>
                <div class="quote">
                    <p>&ldquo;{!! $post->message !!}&rdquo;</p>
                </div>
            </div>
            @endif

	    </div>
	</article>
    <div class="article-infos">
    	@if($tags != "")
        <h2>Post tags</h2>
        <hr/>
        <div class="tags">
            <ul>
            	@foreach($tags as $tag)
            	<li><a href="{!! URL::to('/search/' . $tag) !!}">{!! $tag !!}</a></li>
            	@endforeach
            </ul>
        </div>
        @endif

        @if($post->allow_comment)
        <div class="comments-counter">
            <div class="text">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a href="#normalComments">{!! $post->commentCount() !!} comments</a></li>
                </ul>
            </div>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="normalComments">
                <div class="comments-wrap">
                    <ul>
                    	@foreach($comments as $comment)

                    	<li>
                            <div class="comment" id="{!! $comment->id !!}">
                                <figure>
                                    <img src="{!! URL::asset('ct-themes/img/comment-user01.png') !!}" alt=""/>
                                </figure>
                                <div class="comment-text">
                                    <h3><a href="#">{!! $comment->user->username !!}</a></h3>
                                    <p>{!! $comment->created_at->format('d/m/Y') !!}</p>
                                    <div class="like-box">
                                        <a class="upvote-comment" href="" data-toogle="tooltip" title="Upvote this comment" user-id="{!! Auth::user()->id !!}" comment-id="{!! $comment->id !!}"
                                        style="{!! $comment->isVoted(Auth::user()->id) == 0? 'display:initial' : 'display:none' !!}">
                                            <i class="fa fa-thumbs-o-up"></i> 
                                            <span>{!! $comment->reputation !!}</span> reps
                                        </a>
                                        <a class="upvoted-comment" href="" data-toogle="tooltip" title="You have upvoted this comment" user-id="{!! Auth::user()->id !!}" comment-id="{!! $comment->id !!}"
                                        style="color: #C3092D; {!! $comment->isVoted(Auth::user()->id)? 'display:initial' : 'display:none' !!}">
                                            <i class="fa fa-thumbs-o-up"></i> 
                                            <span>{!! $comment->reputation !!}</span> reps
                                        </a>
                                         &sdot; 
                                        @if($comment->user->id == Auth::user()->id)
                                        <a class="delete-comment" href="#" data-href="{!! $comment->id !!}" data-toggle="modal" data-target="#confirm-delete-comment">delete comment</a>
                                        @endif
                                    </div>
                                    <hr/>
                                    <div class="message">
                                        <p>{!! $comment->content !!}</p>
                                    </div>
                                    {{-- <div class="controls-box">
                                        <a href="#" class="button"><i class="icon-quote"></i></a>
                                        <a href="#" class="button"><i class="icon-replay"></i></a>
                                    </div> --}}
                                </div>
                            </div>
                        </li>
                    	@endforeach
                    </ul>
                </div>

                <div id='goto-this'></div>
                @if( Auth::check() )
                <div>
	                <h2>Post Comment</h2>
	                <hr/>
	                {!! Form::open(array('url' => 'post/postcomment', 'method' => 'post')) !!}
	                <div class="comments-form">
	                	{!! Form::hidden('post_id', $post->id) !!}
	                	{!! Form::textarea('comment', '', array('id' => 'comment', 'class' => 'form-control', 'rows' => 5, 'placeholder' => 'Insert your comment here..')) !!}
	                    {!! Form::submit('Post comment', array('class' => 'btn btn-primary btn-lg custom-button send-btn')) !!}
	                </div>
	                {!! Form::close() !!}
	            </div>
                @endif
            </div>
        </div>
        @else
        <div class="comments-counter">
            <div class="text">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a>Comment feature is disabled.</a></li>
                </ul>
            </div>
        </div>
        @endif
    </div>

    @include('modals.delete-post')
    @include('modals.delete-comment')
    @include('modals.report-post')

@stop