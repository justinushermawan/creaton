<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Creaton - A Place for Artist</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <link rel="icon" type="image/png" href="{!! URL::asset('ct-themes/favicon.png') !!}">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{!! URL::asset('css/grid-masonry.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('css/font-awesome/css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/slider.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/main.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/jquery.selectBoxIt.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/jquery.mCustomScrollbar.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/fileupload/css/jquery.fileupload.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/fileupload/css/jquery.fileupload-ui.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/jquery.tagsinput.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('ct-themes/css/style.css') !!}">

    <script src="{!! URL::asset('ct-themes/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery-1.10.1.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.oembed.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('js/masonry.pkgd.min.js') !!}"></script>
    <!-- JS by Creaton -->
    <script type="text/javascript" src="{!! URL::asset('js/creaton.js') !!}"></script>


    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=225987124123858";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</head>
<body>
@include('partials._header')
<section id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-9 right-content profile-bg">
                <div class="row">
                    <div class="profile-wrap" style="background-image: url('{{ URL::asset('ct-themes/img/settings-cover.jpg') }}');">
                        <div class="col-sm-9">
                            <div class="outer">
                                <div class="inner">
                                    <figure>
                                        <img src="{{ URL::asset('ct-themes/img/user02.png') }}" alt=""/>
                                    </figure>
                                    <div class="text">
                                        <div class="outer">
                                            <div class="inner">
                                                <a href="#" class="user-name">{{ $fullname }}</a>
                                                <div class="page-name">Settings page</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="outer">
                                <div class="inner">
                                    <div class="counter-item">
                                        <span class="number">{{ $reps }}</span><br/>
                                        <span class="exp">reps</span>
                                    </div>
                                    <div class="counter-item">
                                        <span class="number">{{ $totalposts }}</span><br/>
                                        <span class="exp">posts</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-sm-12">
                <div class="main-wrap">
                    <h1>PUBLIC PROFILE & ACCOUNT</h1>
                    <div class="separator"></div>
                    <div class="user-cover-edit" style="background-image: url('{{ URL::asset('ct-themes/img/user-cover02.jpg') }}');">
                        <div class="col-sm-8 col-md-9">
                            <div class="outer">
                                <div class="inner">
                                    <figure>
                                        <img src="{{ URL::asset('ct-themes/img/user02.png') }}" alt=""/>
                                    </figure>
                                    <div class="text">
                                        <div class="outer">
                                            <div class="inner">
                                                <a href="#" class="user-name">{{ $fullname }}</a>
                                                <div class="page-name">Settings page</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button-container">
                                        <button class="btn btn-primary custom-button btn-sm">Edit</button>
                                        <button class="btn btn-primary custom-button btn-sm">Border: #0e0e0e <i class="color-pick"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-3">
                            <div class="outer">
                                <div class="inner">
                                    <button class="btn btn-primary custom-button btn-md">Edit Profile Background</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::open(array('url' => 'account/settings')) !!}
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="input-group">
                                <span class="input-group-addon">Email:</span>
                                <input type="text" name="email" class="form-control" value="{{ $email }}" required>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">Password:</span>
                                <input type="password" name="password" class="form-control" value="" required>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">Full Name:</span>
                                <input type="text" name="fullname" class="form-control" value="{{ $fullname }}" required>
                            </div>
                            <button class="btn btn-primary custom-button btn-block" type="submit"><i class="glyphicon glyphicon-check"></i> Save Settings</button>
                            <div class="spacer"></div>
                        </div>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <span class="input-group-addon">Username:</span>
                                <input type="text" name="username" class="form-control" value="{{ $username }}" required>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">Title:</span>
                                <input type="text" name="title" class="form-control" value="{{ $title }}" required>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">About Me:</span>
                                <input type="text" name="aboutme" class="form-control" value="{{ $aboutm }}">
                            </div>
                        </div>
                    </div>
                    <h1>Preferences</h1>
                    <div class="separator"></div>
                    <div class="checkbox-line">
                        <div class="custom-checkbox">
                            <input type="checkbox" value="check1" name="check" id="check1" checked />
                            <label for="check1">ANONYMOUS SURFING (YOU CAN POST)</label>
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" value="check1" name="check" id="check2" />
                            <label for="check2">ADULT FILTER</label>
                        </div>
                        <div class="custom-checkbox">
                            <input type="checkbox" value="check1" name="check" id="check3" />
                            <label for="check3" class="has-popover" data-content="When you scroll down, active gifs will become inactive. This is a life saver - trust us!">ACTIVATE .GIFs ON SCROLL</label>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                </div>
                </div>
            </div>
            <aside class="col-sm-3 left-aside">
                <div class="sidebar-menu">
                    <div class="sidebar-content sidebar-content-category">
                        @include('partials._categories')
                    </div>
                </div>
            </aside>
        </div>
    </div>
</section>
@include('partials._footer')
<a href="#" class="scroll-top">&nbsp;</a>
@include('modals.register-modal')
@include('modals.post-modal')
@include('modals.delete-post')
<div id="scripts">
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery-ui.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.flexslider-min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.selectBoxIt.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.mCustomScrollbar.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.mousewheel.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/vendor/jquery.ui.widget.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/vendor/jquery.tagsinput.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.iframe-transport.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-process.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-image.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-audio.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-video.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/fileupload/js/jquery.fileupload-validate.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/main.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/retina-1.1.0.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/jquery.lazyload.min.js') !!}"></script>
    <script type="text/javascript" src="{!! URL::asset('ct-themes/js/pwstrength.js') !!}"></script>
</div>
</body>
</html>