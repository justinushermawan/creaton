<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Page Not Found</title>
        <meta charset=utf-8>
        <meta name="description" content="" />
        <meta name="keywords" content="" />		
        <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->   

        <link href='http://fonts.googleapis.com/css?family=Corben' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Nobile' rel='stylesheet' type='text/css'>

        <link href="{{ URL::asset('ct-themes/css/404.css') }}" type="text/css" media="all" rel="stylesheet" />
        <link href="{{ URL::asset('ct-themes/css/bootstrap.min.css') }}" type="text/css" media="all" rel="stylesheet" />
    </head>

    <body>
        <section class="container">
            <div class="row">
                <div class="span8 offset2 main-content">

                    <div id="error">
                        Sorry, this page isn't available
                    </div>
                    <div class="top">
                        <nav id="main-nav">
                            <ul>
                                <li>
                                    <a href="{{ URL::to('/') }}">Go to the Creaton homepage</a>
                                </li>
                            </ul> 
                        </nav>
                    </div>
                    <div class="page-content">
                        The link you followed may be broken, or the page may have been removed.
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>