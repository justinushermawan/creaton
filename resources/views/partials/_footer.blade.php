<div id="footer-container">
    <div class="container">
        <div id="footer">
            <div id="footer-right-container">
                <div id="footer-follow-caption">FOLLOW US : </div>
                <a href="#" target="_blank" title="Facebook">
                    <div id="footer-follow-fb"></div>
                </a>
                <a href="#" target="_blank" title="Twitter">
                    <div id="footer-follow-twitter"></div>
                </a>
                <a href="#" target="_blank" title="Instagram">
                    <div id="footer-follow-ig"></div>
                </a>
                <a href="#" target="_blank" title="Linked In">
                    <div id="footer-follow-li"></div>
                </a>
                <a href="#" target="_blank" title="Google Plus">
                    <div id="footer-follow-gplus"></div>
                </a>
            </div>
        </div>
    </div>
</div>