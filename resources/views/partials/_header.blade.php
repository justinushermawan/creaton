<header>
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand">
                <img src="{!! URL::asset('ct-themes/img/creatonlogo.png') !!}" alt="Creaton"/>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{ URL::to('/')}}">Home</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Creations <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{!! URL::to('/c/image') !!}">Image</a></li>
                        <li><a href="{!! URL::to('/c/video') !!}">Video</a></li>
                        <li><a href="{!! URL::to('/c/text') !!}">Text</a></li>
                    </ul>
                </li>
                <!--<li><a href="{{ URL::to('')}}">Popular</a></li>
                <li><a href="{{ URL::to('')}}">Staff Picks</a></li>
                <li><a href="{{ URL::to('')}}">Creators</a></li>-->
            </ul>
        </div>
        <div class="right-container">
            <ul class="nav navbar-nav navbar-right">
                <li class="search-box">        
                    <form role="search" class="navbar-form navbar-left">
                        <div class="form-group">
                            <input id="search-bar" type="text" placeholder="Search creations.." class="form-control" width="220">
                        </div>
                    </form>
                </li>
                <li><a href="#postModal" data-toggle="modal"><span class="glyphicon glyphicon-pencil"></span> Post New Art</a></li>
                <li>
                    <div class="user-box">
                        <a href="#">
                            <figure>
                                <img src="{!! URL::asset('ct-themes/img/user-box.png') !!}" alt=""/>
                            </figure>
                            <span class="name">{!! Auth::user()->username !!}</span>
                        </a>
                        <div class="drop-down">
                            <div class="buttons-line">
                                <a href="{{ URL::to('/' . Auth::user()->username) }}" class="btn btn-primary btn-block custom-button">Profile</a>
                                <a href="{{ URL::to('/account/settings') }}" class="btn btn-primary btn-block custom-button">Settings</a>
                                <a href="{!! URL::to('account/logout') !!}" class="btn btn-primary btn-block custom-button">Log Out</a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>