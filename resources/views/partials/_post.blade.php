<article class="main-post" id="{!! $post->id !!}">
    <div class="article-top">
        <h1>{!! HTML::link('post/' . $post->slug, $post->title) !!}</h1>
        <hr />
        <div class="counters-line">
            <div class="pull-left">
                <div class="date"><i class="fa fa-calendar"></i>{!! $post->created_at->format('d/m/Y') !!}</div>
                <div class="user"><i class="fa fa-user"></i> <a href="{!! URL::to('/'.$post->user->username) !!}">{!! $post->user->username !!}</a></div>
                <div class="views"><i class="fa fa-eye"></i>{!! $post->total_views !!} views</div>
                @if($post->allow_comment)
                <div class="comments">
                    <i class="fa fa-comment"></i> 
                    <a href="{!!'post/' . $post->id!!}">{!! $post->commentCount() !!}</a> comments
                </div>
                @endif
                <div class="like"><a href="#"><i class="fa fa-thumbs-o-up"></i>{!! $post->reputation !!}</a> reps</div>
            </div>
            <div class="pull-right">
                @if($post->nsfw)
                <div class="corner-tag red">NSFW</div>
                @endif
            </div>
        </div>
        <div class="buttons-bar">
            <div class="buttons">
                <a href="" class="report has-tooltip" data-title="Report post" data-href="{!! $post->id !!}" data-toggle="modal" data-target="#report-post" style="{!! $post->isReportedBy(Auth::user()->id)? 'display:none' : 'display: initial' !!}">
                    <i class="fa fa-flag-o"></i>
                </a>
                <a class="reported has-tooltip" data-title="Post reported" style="{!! $post->isReportedBy(Auth::user()->id) == 0? 'display:none' : 'display: initial' !!}">
                    <i class="fa fa-flag"></i>
                </a>
            </div>
            
            <div class="social-icons">
                {{-- Show upvoted button if user has voted --}}
                <a href="" class="upvoted has-tooltip" data-title="Upvoted" style="{!! $post->isVoted(Auth::user()->id)? 'display:initial' : 'display:none' !!}"><i user-id='{!! Auth::user()->id !!}' post-id='{!! $post->id !!}' class="fa fa-thumbs-up"></i></a>
                {{-- Show upvote button if user has not voted yet --}}
                <a href="" class="upvote has-tooltip" data-title="Upvote" style="{!! $post->isVoted(Auth::user()->id) == 0? 'display:initial' : 'display:none' !!}"><i user-id='{!! Auth::user()->id !!}' post-id='{!! $post->id !!}' class="fa fa-thumbs-o-up"></i></a>

                <a href="javascript:void(0)" class='facebook has-tooltip' data-title="Share on Facebook"><i class="fa fa-facebook"></i></a>
                <a href="javascript:void(0)" class='twitter has-tooltip' data-title="Share on Twitter"><i class="fa fa-twitter"></i></a>
                <a href="javascript:void(0)" class='googleplus has-tooltip' data-title="Share on Google +"><i class="fa fa-google-plus"></i></a>
                {{-- Show delete button if the post is posted by this user --}}
                @if($post->user->id == Auth::user()->id)
                <a href="#" class="other has-tooltip" data-title="Delete post" data-href="{!! $post->id !!}-home" data-toggle="modal" data-target="#confirm-delete-post"><i class="fa fa-trash-o"></i></a>
                @endif
            </div>
        </div>
    </div>
    <div class="article-content">

            {{-- IMAGE POST --}}
            @if($post->post_type_id == 1)
            <figure>
                <div class="corner-tag  {!! $post->subcategory->category->color !!}"><a href="{!! URL::to('sc/'.$post->subcategory->slug) !!}">{!! $post->subcategory->name !!}</a></div>
                <img class="lazy" data-original="{!! URL::asset('userdata/' . $post->user->username . '/post/' . $post->file_path) !!}" alt="Sorry.. This image has been removed :("/>
            </figure>

            {{-- VIDEO POST --}}
            @elseif($post->post_type_id == 2)
            <figure>
                <div class="corner-tag  {!! $post->subcategory->category->color !!}"><a href="{!! URL::to('sc/'.$post->subcategory->slug) !!}">{!! $post->subcategory->name !!}</a></div>
                <div class="{!! $post->id !!} video-post">
                    <input type="hidden" id="video{!! $post->id !!}" class="post_video_url" value="{!! $post->video_url !!}">
                    <!-- Get Post ID -->
                    <input type="hidden" id="{!! $post->id !!}" class="post_video_id">
                    <iframe width="560" height="315" frameborder="0" allowfullscreen></iframe>
                    <script type="text/javascript">
                        var post_id = $('.post_video_url').attr('id');
                        var post_number = post_id + '';
                        var url = $('#' + post_number + '').attr('value');
                        loadVideoPost(url, $('.post_video_id').attr('id'));
                    </script>
                </div>
            </figure>

            {{-- TEXT POST --}}
            @elseif($post->post_type_id == 3)
            <div class="quote-wrap">
                <div class="corner-tag {!! $post->subcategory->category->color !!}"><a href="{!! URL::to('sc/'.$post->subcategory->slug) !!}">{!! $post->subcategory->name !!}</a></div>
                <h2>Message</h2>
                <div class="quote">
                    <p>&ldquo;{!! $post->message !!}&rdquo;</p>
                </div>
            </div>
            @endif
    </div>
</article>