<div class="top-menu">
    <div class="section-title">Creation Categories</div>
</div>
@foreach($categories as $category)
    
<div class="menu-group">
    <h2><a href="{!! URL::to('c/'.$category->slug) !!}">{!! $category->name !!}</a></h2>
    <hr />
    <ul>
        <?php $subs = $category->subcategories()->get(); ?>
        @foreach($subs as $sub)
            <li><a href="{!! URL::to('/sc/'.$sub->slug) !!}">{!! $sub->name !!}</a></li>
        @endforeach
    </ul>
</div>

@endforeach