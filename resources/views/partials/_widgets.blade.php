<aside class="col-sm-3">
    <div class="widget">
        <h2>Daily Top Creator</h2>
        <hr />
        <div class="widget-content">
            <div class="joker">
                <figure>
                    <img src="{!! URL::asset('userdata/'.$topCreator->username.'/profile-picture/user.png') !!}" alt=""/>
                </figure>
                <div class="text">
                    <div class="name"><a href="{!! URL::to('/'.$topCreator->username) !!}">{!! $topCreator->fullname !!}</a></div>
                    <div class="likes">{!! $topCreator->reputation !!} reps</div>
                </div>
                <a href="{!! URL::to('/'.$topCreator->username) !!}" class="btn btn-primary btn-block custom-button">See Profile</a>
            </div>
        </div>
    </div>
    <div class="widget">
        <h2><a href="#">Top 5 Creations</a></h2>
        <hr />
        <div class="widget-content">
            <div class="post-list">
                <article>
                    <figure>
                        <img src="{!! URL::asset('ct-themes/img/top1.jpg') !!}" alt=""/>
                        <figcaption>01</figcaption>
                    </figure>
                    <div class="text">
                        <h3><a href="#">Helping the elderly is not fun...</a></h3>
                        <span class="date">25.04</span>
                    </div>
                </article>
                <article>
                    <figure>
                        <img src="{!! URL::asset('ct-themes/img/top2.jpg') !!}" alt=""/>
                        <figcaption>02</figcaption>
                    </figure>
                    <div class="text">
                        <h3><a href="#">People Cherising New Year's Eve
</a></h3>
                        <span class="date">25.04</span>
                    </div>
                </article>
                <article>
                    <figure>
                        <img src="{!! URL::asset('ct-themes/img/top3.jpg') !!}" alt=""/>
                        <figcaption>03</figcaption>
                    </figure>
                    <div class="text">
                        <h3><a href="#">Inappropriate jokes are alway...</a></h3>
                        <span class="date">23.04</span>
                    </div>
                </article>
                <article>
                    <figure>
                        <img src="{!! URL::asset('ct-themes/img/top4.jpg') !!}" alt=""/>
                        <figcaption>04</figcaption>
                    </figure>
                    <div class="text">
                        <h3><a href="#">The Law</a></h3>
                        <span class="date">22.04</span>
                    </div>
                </article>
                <article>
                    <figure>
                        <img src="{!! URL::asset('ct-themes/img/top5.jpg') !!}" alt=""/>
                        <figcaption>05</figcaption>
                    </figure>
                    <div class="text">
                        <h3><a href="#">How Photoshop works</a></h3>
                        <span class="date">22.04</span>
                    </div>
                </article>
            </div>
        </div>
    </div>
</aside>