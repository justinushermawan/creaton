
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/* Validator */
$(document).ready(function() {
	$('#username').blur(validateName);
	$('#email').keyup(validateEmailAddress);
	$('#rp-password').keyup(validateConfirmPassword);
});

/* Validate Name */
function validateName() {
	var name = $('#username').val();

	if (name == '') {
		$('#username-status').show();
		$('#username-status').html('<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> Choose a username for you.');
	} else {
		$('#username-status').hide();
	}
}

/* Validate Email Address */
function validateEmailAddress() {
	var emailAddress = $('#email').val();
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    
    if (emailAddress == '') {
		$('#email-status').show();
		$('#email-status').html('<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> What\'s your email address?');
	} else {
		if (!pattern.test(emailAddress)) {
			$('#email-status').show();
			$('#email-status').html('<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> Your email format is not valid.');
    	} else {
			$('#email-status').hide();
		}
	}
};

/* Validate Password Strength */
/*
$(document).ready(function() {
	"use strict";
	var options = {
		onLoad: function() {
		},
		onKeyUp: function(evt) {
			$(evt.target).pwstrength("outputErrorList");
		}
	};
	$('#password').pwstrength(options);
});
*/

/* Validate Confirm Password */
function validateConfirmPassword() {
	var password = $('#password').val();
	var rppassword = $('#rp-password').val();

	if (rppassword != '') {
		if (password != rppassword) {
			$('#rp-password-status').show();
			$('#rp-password-status').html('<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> Password does not match.');
		} else {
			$('#rp-password-status').hide();
		}
	} else {
		$('#rp-password-status').hide();
	}
}

function validatePost() {
	var category = $('#categoryCombobox').val();

	if(category == "null") {
		alert('Please select the art category');
		return false;
	}

	var postType = $('#myCombobox').val();
	var title, message, image, video;

	if(postType == 3) {
		// text
		title = $('#title-text').val();

		alert(title);
		if(title == "") {
			alert('Please insert the art title');
			return false;
		}
		message = $('#message').val();
		if(message == "") {
			alert('Please insert the message text');
			return false;
		}
	}
	else if(postType == 1) {
		// image
		title = $('#title-image').val();
		if(title == "") {
			alert('Please insert the art title');
			return false;
		}
		image = $('#photoimg').val();
		if(image == ""){
			alert('Please insert the image');
			return false;	
		}
	}
	else if(postType == 2) {
		// video
		title = $('#title-video').val();
		if(title == "") {
			alert('Please insert the art title');
			return false;
		}
		video = $('#video-url').val();
		if(video == "") {
			alert('Please insert the video URL');
			return false;
		}
	}
	return true;
}

/* Video Embed */
$(document).ready(function() {
    var last_cnad_text_1 = '';
    var options_cnad_text_1 = {
    	embedMethod: 'fill',
    	maxWidth: 560,
    	maxHeight: 315
    };

    function loadVideo()
    {
    	val = $('#video-url').val();
    	if ( val != '' && val != last_cnad_text_1 )
    	{
   			last_cnad_text_1 = val;
    		$("#video-wrap").oembed(val, options_cnad_text_1);
    	}
    }

    $(function() {
    	$('#video-url').keydown(loadVideo);
		$('#video-url').click(loadVideo);
		$('#video-url').change(loadVideo);
    });
});

function loadVideoPost(video_url, post_id) {
	var last_cnad_text_1 = '';
    var options_cnad_text_1 = {
    	embedMethod: 'fill'
    };
	if ( video_url != '' && video_url != last_cnad_text_1 )
	{
		last_cnad_text_1 = video_url;
		$('.' + post_id + '').oembed(video_url, options_cnad_text_1);
	}
}

/* Lazy Loading */
var lazyload = lazyload || {};

(function($, lazyload) {

	"use strict";

	var buttonId = "#button-more",
		loadingId = "#loading-div",
		container = ".main-wrap",
		page = 0;

	lazyload.load = function() {
		page = $(buttonId).attr('data-value');
		$(buttonId).hide();
		$(loadingId).show();

		$.ajax({
			url: 'postmore',
			type: 'POST',
			data: 'start=' + page,
			success: function(response) {
				if (!response || response.trim() == "NONE") {
					$(buttonId).fadeOut();
					$(container).append('<div id=\'loading-div\'>No more posts to load.</div>');
					return;
				}
				appendContests(response);
			},
			error: function(response) {
				$(loadingId).text("Whoops, there is something wrong.");
			}
		});

	};

	var appendContests = function(response) {

		$(buttonId).remove();
		$(loadingId).hide();

		$(response).appendTo($(container));

	}

})(jQuery, lazyload);

/* Upload comment */
$(document).ready(function(){
  	$('.send-btn').click(function(e){ 

  		e.preventDefault();
  		$('.send-btn').attr('disabled', 'disabled');

  		var comment = $("[name='comment']").val();
  		var post_id = $("[name='post_id']").val();

  		if(comment == '') {
  			alert('Please insert comment first!');
  			return false;
  		}

		$.post(
			'postcomment',  
			{'comment': comment, 'post_id': post_id, '_token': $("[name='csrf-token']").val()}
		)
		.done(function(data){
			$('.send-btn').removeAttr('disabled');
  			$("[name='comment']").val('');
  			var liData = data;
  			$(liData).appendTo('.comments-wrap ul').fadeIn('slow');
			//$(".comments-wrap ul").append(data).fadeIn('slow');
		})
		.fail(function(err){
			console.log('error');
			console.log(err);
		});
  }); 
});

/* 
Upvote Post
Upvote Comment
Downvote Post
Downvote Comment
Delete Post
Delete Comment 
*/
$(document).ready(function(){

	// Upvote Post
	$('.upvote').click(function(e){
		e.preventDefault();
		var anchor = $(this);
		var button = $(this).find('.fa'); // get element <i></i> to get parameters
		var user_id = button.attr('user-id');
		var post_id = button.attr('post-id');

		$.post(
			'upvotePost',
			{'user_id': user_id, 'post_id': post_id}
		)
		.done(function(data){
			$('#upvote' + post_id).text(data); // update total upvotes
			anchor.css('display', 'none'); // hide button upvote
			anchor.siblings('.upvoted').css('display', 'initial'); // show button downvote
		})
		.fail(function(err){
			alert('Oops, failed to upvote the post! :(');
			console.log(err);
		});
	});

	// Downvote Post
	$('.upvoted').click(function(e){
		e.preventDefault();
		var anchor = $(this);
		var button = $(this).find('.fa'); // get element <i></i> to get parameters
		var user_id = button.attr('user-id');
		var post_id = button.attr('post-id');

		$.post(
			'downvotePost',
			{'user_id': user_id, 'post_id': post_id}
		)
		.done(function(data){
			$('#upvote' + post_id).text(data); // update total upvotes
			anchor.css('display', 'none'); // hide button upvote
			anchor.siblings('.upvote').css('display', 'initial'); // show button downvote
		})
		.fail(function(err){
			alert('Oops, failed to downvote the post! :(');
			console.log(err);
		});
	});

	// Upvote Comment
	$('.upvote-comment').click(function(e){
		e.preventDefault();
		var anchor = $(this);
		var span = $(this).find('span');
		var user_id = anchor.attr('user-id');
		var comment_id = anchor.attr('comment-id');

		$.post(
			'upvoteComment',
			{'user_id': user_id, 'comment_id': comment_id}
		)
		.done(function(data){
			anchor.css('display', 'none');
			var upvote = anchor.siblings('.upvoted-comment').css('display', 'initial');
			span.text(data);
			upvote.find('span').text(data);
		})
		.fail(function(err){
			alert('Oops, failed to upvote the comment! :(');
			console.log(err);
		});
	});

	// Downvote Comment
	$('.upvoted-comment').click(function(e){
		e.preventDefault();
		var anchor = $(this);
		var icon = $(this).find('.fa');
		var span = $(this).find('span');
		var user_id = anchor.attr('user-id');
		var comment_id = anchor.attr('comment-id');
		console.log(anchor, 'downvote');


		$.post(
			'downvoteComment',
			{'user_id': user_id, 'comment_id': comment_id}
		)
		.done(function(data){
			anchor.css('display', 'none');
			var upvote = anchor.siblings('.upvote-comment').css('display', 'initial');
			span.text(data);
			upvote.find('span').text(data);
		})
		.fail(function(err){
			alert('Oops, failed to downvote the comment! :(');
			console.log(err);
		});
	});

	// Show delete post modal
	$('#confirm-delete-post').on('show.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('href');
        $(this).find('.btn-ok').attr('href', id);
        
        $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        $('#btn-delete-post').attr('post-id', id);
    });

	// Delete post
    $('#btn-delete-post').click(function(e){
    	var button = $(this);
    	var temp = $(this).attr('post-id').split('-');
    	var post_id = temp[0];
    	var src = temp[1];

    	$.post(
    		'deletepost',
    		{'post_id' : post_id, 'src': src}
    	)
    	.done(function(data){
    		if(src == 'detail'){
    			location.href = data;
    		}
    		else if(src == 'home'){
    			console.log('success home');
    			$("#" + post_id).animate({height: 'toggle', opacity: 'toggle'}, 500);
    		}
    	})
    	.fail(function(err){
    		console.log(err);
    		alert('Oops, failed to delete post! :(');
    	})
    });

    // Show delete comment modal
	$('#confirm-delete-comment').on('show.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('href');
        $(this).find('.btn-ok').attr('href', id);
        
        $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        $('#btn-delete-comment').attr('comment-id', id);
    });

    // Delete comment
    $('#btn-delete-comment').click(function(e){
    	e.preventDefault();

    	var button = $(this);
    	var comment_id = $(this).attr('comment-id');

    	$.post(
    		'deletecomment',
    		{'comment_id' : comment_id}
		)
		.done(function(data){
			console.log(data);
			if(data == 'true')
			{
				// succeed -> delete div
				//button.parentsUntil('li').animate({height: 'toggle', opacity: 'toggle'}, 500);
				$("#" + comment_id).animate({height: 'toggle', opacity: 'toggle'}, 500);
			}
			else
			{
				// failed -> give alert
				alert('Oops, failed to delete comment! :(');
			}
		})
		.fail(function(err){
			alert('Oops, there is a problem when delete comment! :(');
			console.log(err);
		});
    });
});

/*
Search with enter
 */
$(document).ready(function(){
	$("#search-bar").keypress(function(e){
		if(e.which == 13){
			e.preventDefault();
			if($(this).val() == ''){
				alert('Insert some words to look up for');
			}
			else {
				var url = location.href;
				var temp = url.split('/');
				location.href = "http://" + temp[2] + "/" + temp[3] + "/search/" + $(this).val();
			}
			return false;
		}
	});
});

/*
Report post
 */
$(document).ready(function(){

	// Show report post modal
	$('#report-post').on('show.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('href');
        $('#btn-report-post').attr('post-id', id);
    });

	// Confirm report clicked
	$('#btn-report-post').click(function(e){
		e.preventDefault();

		var post_id = $(this).attr('post-id');
		var reason = $('#report-post-reason').val();
		var description = $('#report-post-description').val();

		$.post(
			'reportPost',
			{'post_id': post_id, 'reason': reason, 'description': description}
		)
		.done(function(data){
			location.href = data;
		})
		.fail(function(err){
			console.log(err);
			alert('Oops, failed to report the post! :(');
		})
	});
});

/* Masonry grid */
$(document).ready(function(){
	$('.m-grid').masonry({
		itemSelector: '.pin',
		columnWidth: '.pin',
    	percentPosition: true
	});
})

$(document).ready(function(){
	$('#myCombobox').change(function(){
		var items = "";
		var postType = $(this).val();
		$.getJSON(window.location.origin + '/creaton.git/getSubcategory/' + postType, function(data){
			$("#categoryCombobox").data("selectBox-selectBoxIt").remove();
			$.each(data, function(index, item){
				$('#categoryCombobox').data('selectBox-selectBoxIt').add({value: item.id, text: item.name});
			});
		});
	});
})