<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Comment extends Model {

	protected $table = 'comments';

	protected $fillable = ['user_id', 'post_id', 'content', 'reputation', 'audit_status'];

	protected $guard = ['id'];

	public function post()
	{
		return $this->belongsTo('App\Post');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function isVoted($user_id)
	{
		return DB::table('comment_upvotes')
					->where('comment_id', '=', $this->id)
					->where('user_id', '=', $user_id)
					->count();
	}

}
