<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PostUpvote extends Model {

	protected $table = 'post_upvotes';

	protected $fillable = ['user_id', 'post_id', 'audit_status'];

	protected $guard = ['id'];

}
