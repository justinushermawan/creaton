<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubCategory extends Model {

	protected $table = 'subcategories';

	protected $fillable = ['slug', 'name', 'category_id', 'audit_status'];

	protected $guarded = ['id'];

	public function category()
	{
		return $this->belongsTo('App\Category');
	}

	public function postCount()
	{
		return DB::table('posts')->where('subcategory_id', '=', $this->id)->where('audit_status', '!=', 'D')->count();
	}

}
