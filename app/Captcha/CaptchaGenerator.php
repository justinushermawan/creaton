<?php namespace App\Captcha;

	class CaptchaGenerator {

		public static function showCaptcha()
		{
			define("SERVER_URL", "http://www.google.com/recaptcha/api");
			define("PUBLIC_KEY", "6LcipQcTAAAAAFOyOEsycXF24NMzZ3wTsrJzR6X5");
	      	$script = "<script>var RecaptchaOptions = {theme: 'custom', custom_theme_widget: 'recaptcha_widget'};</script>\n";
	      	$html = '<script type="text/javascript" src="'. SERVER_URL . '/challenge?k=' . PUBLIC_KEY . '"></script><noscript>
	          <iframe src="'. SERVER_URL . '/noscript?k=' . PUBLIC_KEY  . '" height="300" width="500" frameborder="0"></iframe><br/>
	          <textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
	          <input type="hidden" name="recaptcha_response_field" value="manual_challenge"/>
	      	</noscript>';
	      	echo $script.$html;
		}

	}