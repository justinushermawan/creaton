<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PostReport extends Model {

	protected $table = 'post_reports';

	protected $fillable = ['user_id', 'post_id', 'reason', 'description', 'audit_status'];

	protected $guard = ['id'];

	

}
