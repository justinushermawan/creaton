<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PostType extends Model {

	protected $table = 'post_types';

	protected $fillable = ['name', 'audit_status'];

	protected $guarded = ['id'];

}
