<?php

/*
 * Describe you custom columns and form items here.
 *
 * There is some simple examples what you can use:
 *
 *		Column::register('customColumn', '\Foo\Bar\MyCustomColumn');
 *
 * 		FormItem::register('customElement', \Foo\Bar\MyCustomElement::class);
 *
 * 		FormItem::register('otherCustomElement', function (\Eloquent $model)
 * 		{
 *			AssetManager::addStyle(URL::asset('css/style-to-include-on-page-with-this-element.css'));
 *			AssetManager::addScript(URL::asset('js/script-to-include-on-page-with-this-element.js'));
 * 			if ($model->exists)
 * 			{
 * 				return 'My edit code.';
 * 			}
 * 			return 'My custom element code';
 * 		});
 */
	AdminRouter::get('Posts/{id}', ['uses'=>'\SleepingOwl\Admin\Controllers\PostController@index']);
	AdminRouter::get('updatePost/{id}', ['uses'=>'\SleepingOwl\Admin\Controllers\PostController@updatePost']);
	AdminRouter::post('doUpdatePost/{id}', ['uses'=>'\SleepingOwl\Admin\Controllers\PostController@doUpdatePost']);
	AdminRouter::get('insertPost', ['uses'=>'\SleepingOwl\Admin\Controllers\PostController@insertPost']);
	AdminRouter::post('doInsertPost', ['uses'=>'\SleepingOwl\Admin\Controllers\PostController@doInsertPost']);
	AdminRouter::get('doDeletePost/{id}', ['uses'=>'\SleepingOwl\Admin\Controllers\PostController@doDeletePost']);
	AdminRouter::get('updateCategory/{id}', ['uses'=>'\SleepingOwl\Admin\Controllers\CategoryController@updateCategory']);
	AdminRouter::post('doUpdateCategory/{id}', ['uses'=>'\SleepingOwl\Admin\Controllers\CategoryController@doUpdateCategory']);
	AdminRouter::get('insertCategory', ['uses'=>'\SleepingOwl\Admin\Controllers\CategoryController@insertCategory']);
	AdminRouter::post('doInsertCategory', ['uses'=>'\SleepingOwl\Admin\Controllers\CategoryController@doInsertCategory']);
	AdminRouter::get('doDeleteCategory/{id}', ['uses'=>'\SleepingOwl\Admin\Controllers\CategoryController@doDeleteCategory']);
	AdminRouter::get('updateSubCategory/{id}', ['uses'=>'\SleepingOwl\Admin\Controllers\SubCategoryController@updateSubCategory']);
	AdminRouter::post('doUpdateSubCategory/{id}', ['uses'=>'\SleepingOwl\Admin\Controllers\SubCategoryController@doUpdateSubCategory']);
	AdminRouter::get('insertSubCategory', ['uses'=>'\SleepingOwl\Admin\Controllers\SubCategoryController@insertSubCategory']);
	AdminRouter::post('doInsertSubCategory', ['uses'=>'\SleepingOwl\Admin\Controllers\SubCategoryController@doInsertSubCategory']);
	AdminRouter::get('doDeleteSubCategory/{id}', ['uses'=>'\SleepingOwl\Admin\Controllers\SubCategoryController@doDeleteSubCategory']);

?>
