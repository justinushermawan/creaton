<?php

/*
 * Describe your menu here.
 *
 * There is some simple examples what you can use:
 *
 * 		Admin::menu()->url('/')->label('Start page')->icon('fa-dashboard')->uses('\AdminController@getIndex');
 * 		Admin::menu(User::class)->icon('fa-user');
 * 		Admin::menu()->label('Menu with subitems')->icon('fa-book')->items(function ()
 * 		{
 * 			Admin::menu(\Foo\Bar::class)->icon('fa-sitemap');
 * 			Admin::menu('\Foo\Baz')->label('Overwrite model title');
 * 			Admin::menu()->url('my-page')->label('My custom page')->uses('\MyController@getMyPage');
 * 		});
 */

Admin::menu()->url('/')->label('Start page')->icon('fa-dashboard')->uses('\SleepingOwl\Admin\Controllers\DummyController@getIndex');
Admin::menu()->url('Posts')->label('Post')->icon('fa-comment')->uses('\SleepingOwl\Admin\Controllers\PostController@index');
Admin::menu()->url('Categories')->label('Category')->icon('fa-book')->uses('\SleepingOwl\Admin\Controllers\CategoryController@index');
Admin::menu()->url('Sub_Categories')->label('Sub Category')->icon('fa-sitemap')->uses('\SleepingOwl\Admin\Controllers\SubCategoryController@index');
// Admin::menu()->url('Post_Report')->label('Post Report')->icon('fa-exclamation')->uses('\SleepingOwl\Admin\Controllers\PostReportController@index');