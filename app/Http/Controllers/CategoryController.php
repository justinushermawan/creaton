<?php namespace App\Http\Controllers;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Post;
use App\PostType;
use App\SubCategory;
use App\User;
use Illuminate\Http\Request;

class CategoryController extends Controller {


	public function show($slugCat)
	{
		$cat = Category::whereSlug($slugCat)->where('audit_status', '!=', 'D')->firstOrFail();

		$child = SubCategory::where('category_id', '=', $cat->id)->where('audit_status', '!=', 'D')->get();

		$subCount = count($child);

		// Get All of Category Data
		$categories = Category::all();

		// Get Text Type of Sub-Category Data
		$subs = SubCategory::where('category_id', '=', 3)->where('audit_status', '!=', 'D')->get();

		// Get All of Post Type Data
		$types = PostType::all();

		$topCreator = User::orderBy('reputation', 'desc')->first();

		return \View::make('frontend-blades.category')
					->with('category', $cat)
					->with('subCount', $subCount)
					->with('childSubs', $child)
					->with('categories', $categories)
					->with('subcategories', $subs)
					->with('posttypes', $types)
					->with('topCreator', $topCreator);
	}

	/**
	 * Display the SubCategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showSub($slugSub)
	{
		$sub = SubCategory::whereSlug($slugSub)->where('audit_status', '!=', 'D')->with('category')->firstOrFail();

		$posts = Post::where('subcategory_id', '=', $sub->id)->where('audit_status', '!=', 'D')->get();

		$postCount = count($posts);

		// Get All of Category Data
		$categories = Category::all();

		// Get Text Type of Sub-Category Data
		$subs = SubCategory::where('category_id', '=', 3)->where('audit_status', '!=', 'D')->get();

		// Get All of Post Type Data
		$types = PostType::all();

		$topCreator = User::orderBy('reputation', 'desc')->first();

		return \View::make('frontend-blades.subcategory')
					->with('sub', $sub)
					->with('postCount', $postCount)
					->with('posts', $posts)
					->with('categories', $categories)
					->with('subcategories', $subs)
					->with('posttypes', $types)
					->with('topCreator', $topCreator);
	}

}
