<?php namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\PostType;
use App\SubCategory;
use App\User;
use Auth;
use HTML;
use Str;
use URL;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$start = 0;
		$totalPost = 10;
		// Get All of Category Data
		$categories = Category::all();

		// Get Text Type of Sub-Category Data
		$subs = SubCategory::where('category_id', '=', 3)->where('audit_status', '!=', 'D')->get();

		// Get All of Post Type Data
		$types = PostType::all();

		// Get All of Post Data
		$posts = Post::with('user', 'subcategory')
				->where('audit_status', '!=', 'D')
				->orderBy('created_at', 'DESC')
				->take($totalPost)
				->skip($start)
				->get();

		$topCreator = User::orderBy('reputation', 'desc')->first();
				
        return \View::make("frontend-blades.home")
        	->with("categories", $categories)
        	->with("subcategories", $subs)
        	->with("posttypes", $types)
        	->with("posts", $posts)
        	->with("topCreator", $topCreator);
	}

	public function loadMore()
	{
		$start = $_POST['start'];
		$totalPost = 10;
		$posts = Post::with('user', 'subcategory')
				     ->with('subcategory.category')
				     ->orderBy('created_at', 'DESC')
				     ->where('audit_status', '!=', 'D')
				     ->take($totalPost)
				     ->skip($start)
				     ->get();

		$strHTML = "";

		foreach($posts as $post){

			$strPost = "";
			if($post->post_type_id == 1)
			{
				$asset = 'userdata/' . $post->user->username . '/post/' . $post->file_path . '';
				// Image Posts
				$strPost .= 
				'<figure>
                    <div class=\'corner-tag ' . $post->subcategory->category->color . '\'><a href=\'' . URL::to('sc/'.$post->subcategory->slug) . '\'>' . $post->subcategory->name . '</a></div> 
                    <img class=\'lazy\' data-original=\'' . URL::asset($asset) . '\' src=\'' . URL::asset($asset) . '\' alt=\'Sorry.. This image has been removed :(\' />
                </figure>';
			}
			else if($post->post_type_id == 2)
			{
				// Video Posts
				$strPost .=
				'<figure>
                    <div class=\'corner-tag ' . $post->subcategory->category->color . '\'><a href=\'' . URL::to('sc/'.$post->subcategory->slug) . '\'>' . $post->subcategory->name . '</a></div>
                    <div class=\'' . $post->id . ' video-post\'>
                        <input type=\'hidden\' id=\'video' . $post->id . '\' class=\'post_video_url\' value=\'' . $post->video_url .'\'>
                        <!-- Get Post ID -->
                        <input type=\'hidden\' id=\'' . $post->id . '\' class=\'post_video_id\'>
                        <iframe width=\'560\' height=\'315\' frameborder=\'0\' allowfullscreen></iframe>
                        <script type=\'text/javascript\'>
                            var post_id = $(\'.post_video_url\').attr(\'id\');
                            var post_number = post_id + \'\';
                            var url = $(\'#\' + post_number + \'\').attr(\'value\');
                            loadVideoPost(url, $(\'.post_video_id\').attr(\'id\'));
                        </script>
                    </div>
                </figure>';
			}
			else if($post->post_type_id == 3)
			{
				// Text Posts
				$strPost .= 
				'<div class=\'quote-wrap\'>
                    <div class=\'corner-tag ' . $post->subcategory->category->color . '\'><a href=\'' . URL::to('sc/'.$post->subcategory->slug) . '\'>' . $post->subcategory->name . '</a></div>
                    <h2>Message</h2>
                    <div class=\'quote\'>
                        <p>&ldquo;' . $post->message . '&rdquo;</p>
                    </div>
                </div>';
			}

			$strNSFW = '';
			if($post->nsfw)
			{
				$strNSFW = 
				'<div class=\'pull-right\'>
	          		<div class=\'corner-tag red\'>NSFW</div>
                </div>';
			}

			$strComment = '';
			if($post->comments)
			{
				$strComment =
				'<div class=\'comments\'>
                    <i class=\'fa fa-comment\'></i> 
                    <a href=\'' . 'post/' . $post->id . '\'>' . $post->commentCount() . '</a> comments
                </div>';
			}

			$strStyleUpvote = '';
			$strStyleDownvote = '';

			if($post->isVoted(Auth::user()->id))
			{
				$strStyleUpvote = 'display:none';
				$strStyleDownvote = 'display:initial';
			}
			else
			{
				$strStyleUpvote = 'display:initial';
				$strStyleDownvote = 'display:none';
			}

			$strStyleReport = '';
			$strStyleReported = '';
			if($post->isReportedBy(Auth::user()->id))
			{
				$strStyleReport = 'display:none';
				$strStyleReported = 'display:initial';
			}
			else
			{
				$strStyleReport = 'display:initial';
				$strStyleReported = 'display:none';
			}

			$strDeleteButton = '';
			if($post->user->id == Auth::user()->id)
			{
				$strDeleteButton = '<a href=\'#\' class=\'other has-tooltip\' data-title=\'Delete post\' data-href=\'' . $post->id . '-home\' data-toggle=\'modal\' data-target=\'#confirm-delete-post\'><i class=\'fa fa-trash-o\'></i></a>';
			}

			$strHTML .= 
			'<article class=\'main-post\' id=\'' . $post->id . '\'>
	            <div class=\'article-top\'>
		                <h1><a href=\'' . URL::to('post/'.$post->slug) . '\'>' . $post->title . '</a></h1>
	                	<hr />
	                <div class=\'counters-line\'>
	                    <div class=\'pull-left\'>
	                        <div class=\'date\'><i class=\'fa fa-calendar\'></i>' . $post->created_at->format('d/m/Y') . '</div>
	                        <div class=\'user\'><i class=\'fa fa-user\'></i> <a href=\'profile.html\'>' . $post->user->username . '</a></div>
	                        <div class=\'views\'><i class=\'fa fa-eye\'></i>' . $post->total_views . ' views</div>
	                        ' . $strComment . '
	                        <div class=\'like\'><a href=\'#\'><i class=\'fa fa-thumbs-o-up\'></i>' . $post->reputation . '</a> reps</div>
	                    </div>
	                    ' . $strNSFW . '
	                </div>
	                <div class=\'buttons-bar\'>
	                    <div class=\'buttons\'>
	                        <a href=\'\' class=\'report has-tooltip\' data-title=\'Report post\' data-href=\'' . $post->id . '\' data-toggle=\'modal\' data-target=\'#report-post\' style=\'' . $strStyleReport . '\'>
	                            <i class=\'fa fa-flag-o\'></i>
	                        </a>
	                        <a class=\'reported has-tooltip\' data-title=\'Post reported\' style=\'' . $strStyleReported . '\'>
	                            <i class=\'fa fa-flag\'></i>
	                        </a>
	                    </div>
	                    <div class=\'social-icons\'>

	                        <a href=\'\' class=\'upvoted has-tooltip\' data-title=\'Upvoted\' style=\'' . $strStyleDownvote . '\'><i user-id=\'' . Auth::user()->id . '\' post-id=\'' . $post->id . '\' class=\'fa fa-thumbs-up\'></i></a>
	                        <a href=\'\' class=\'upvote has-tooltip\' data-title=\'Upvote\' style=\'' . $strStyleUpvote . '\'><i user-id=\'' . Auth::user()->id . '\' post-id=\'' . $post->id . '\' class=\'fa fa-thumbs-o-up\'></i></a>

	                        <a href=\'javascript:void(0)\' data-href=\'https:\/\/www.facebook.com\/sharer\/sharer.php?u=http:\/\/teothemes.com\/html\/Aruna\' class=\'facebook has-tooltip\' data-title=\'Share on Facebook\'><i class="fa fa-facebook"></i></a>
	                        <a href=\'javascript:void(0)\' data-href=\'https:\/\/twitter.com\/intent\/tweet?source=tweetbutton&amp;text=Check the best image\/meme sharing website!&url=http:\/\/teothemes.com\/html\/Aruna\' class=\'twitter has-tooltip\' data-title=\'Share on Twitter\'><i class="fa fa-twitter"></i></a>
	                        <a href=\'javascript:void(0)\' data-href=\'https:\/\/plus.google.com\/share?url=http:\/\/teothemes.com\/html\/Aruna\' class=\'googleplus has-tooltip\' data-title=\'Share on Google +\'\'><i class="fa fa-google-plus"></i></a>
	                        ' . $strDeleteButton . '
	                    </div>
	                </div>
	            </div>
	            <div class=\'article-content\'>
	            	' . $strPost . '
	            </div>
	        </article>';
		}

		if (count($posts) > 0) {
			$strHTML .=
				'<button id=\'button-more\' class=\'btn btn-default\' style=\'display:block; width:100%;\' data-value=\'' . ($start + count($posts)) . '\' type=\'button\' onclick=\'lazyload.load()\'>
					Load More
				</button>';
		}
		
		return $strHTML;
	}

}
