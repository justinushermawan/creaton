<?php namespace App\Http\Controllers;

use App\Comment;
use App\CommentUpvote;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Request;
use Response;

class CommentController extends Controller {

	/**
	 * Post comment to a art post
	 * @return Response AJAX response
	 */
	public function post()
	{
		if(Request::ajax()){
			$comment = new Comment;

			$comment->user_id 		= Auth::user()->id;
			$comment->post_id 		= Input::get('post_id');
			$comment->content 		= Input::get('comment');
			$comment->reputation 	= 0;
			$comment->audit_status 	= 'I';

			if($comment->save()){

				$strStyleUpvote = '';
				$strStyleDownvote = '';

				if($comment->isVoted(Auth::user()->id))
				{
					$strStyleUpvote = 'display:none';
					$strStyleDownvote = 'display:initial';
				}
				else
				{
					$strStyleUpvote = 'display:initial';
					$strStyleDownvote = 'display:none';
				}

				$strHTML = 
				'<li>
                    <div class=\'comment\'>
                        <figure>
                            <img src=\'' . URL::asset('ct-themes/img/comment-user01.png') . '\' alt=\'\'/>
                        </figure>
                        <div class=\'comment-text\'>
                            <h3><a href=\'#\'>' . $comment->user->username . '</a></h3>
                            <p>' . $comment->created_at->format('d/m/Y') . '</p>
                            <div class=\'like-box\'>
                                <a class=\'upvote-comment\' href=\'\' data-toogle=\'tooltip\' title=\'Upvote this comment\' user-id=\'{!! Auth::user()->id !!}\' comment-id=\'{!! $comment->id !!}\'
                                style=\'' . $strStyleUpvote . '\'>
                                    <i class=\'fa fa-thumbs-o-up\'></i> 
                                    <span>' . $comment->reputation . '</span> reps
                                </a>
                                <a class=\'upvoted-comment\' href=\'\' data-toogle=\'tooltip\' title=\'You have upvoted this comment\' user-id=\'{!! Auth::user()->id !!}\' comment-id=\'{!! $comment->id !!}\'
                                style=\'color: #C3092D;' . $strStyleDownvote . '\'>
                                    <i class=\'fa fa-thumbs-o-up\'></i> 
                                    <span>' . $comment->reputation . '</span> reps
                                </a>
                                 &sdot; 
                                <a class=\'delete-comment\' href=\'\' comment-id=\'' . $comment->id . '\'>delete comment</a>
                            </div>
                            <hr/>
                            <div class=\'message\'>
                                <p>' . $comment->content . '</p>
                            </div>
                        </div>
                    </div>
                </li>';


				return $strHTML;
			}
			else{
				$response = array('success' => false, 'comment' => 'Error saving comment');
				return Response::json($response, 400);
			}
		}
	}

	public function delete()
	{
		if(Request::ajax())
		{
			$id = Input::get('comment_id');
			$comment = Comment::findOrFail($id);
			$comment->audit_status = 'D';

			if($comment->save())
			{
				return 'true';
			}
			else
			{
				return 'false';
			}
		}
	}

	public function upvote()
	{
		if(Request::ajax())
		{
			$user_id 			= Input::get('user_id');
			$comment_id 		= Input::get('comment_id');

			$upvote 			= new CommentUpvote;
			$upvote->user_id 	= $user_id;
			$upvote->comment_id = $comment_id;

			$comment = Comment::find($comment_id);
			$comment->reputation += 1;
			$comment->save();

			if($upvote->save())
			{
				return $comment->reputation;
			}
			else
			{
				return Response::json(array('success', false), 400);
			}
		}
	}

	public function downvote()
	{
		if(Request::ajax())
		{
			$user_id 	= Input::get('user_id');
			$comment_id = Input::get('comment_id');

			$success = DB::table('comment_upvotes')
							->where('user_id', '=', $user_id)
							->where('comment_id', '=', $comment_id)
							->where('audit_status', '!=', 'D')
							->delete();

			$comment = Comment::find($comment_id);
			$comment->reputation -= 1;
			$comment->save();

			if($success)
			{
				return $comment->reputation;
			}
			else
			{
				return Response::json(array('success', false), 400);
			}
		}
	}

}
