<?php namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\PostType;
use App\SubCategory;
use App\User;
use Auth;

class LandingController extends Controller {

	public function __construct()
	{
		//$this->middleware('guest');
	}

	public function index()
	{
		if (Auth::check())
		{
			$start = 0;
			$totalPost = 10;
			// Get All of Category Data
			$categories = Category::all();

			// Get Text Type of Sub-Category Data
			$subs = SubCategory::where('category_id', '=', 3)->where('audit_status', '!=', 'D')->get();

			// Get All of Post Type Data
			$types = PostType::all();

			// Get All of Post Data
			$posts = Post::with('user', 'subcategory')->orderBy('created_at', 'DESC')->where('audit_status', '!=', 'D')->take($totalPost)->skip($start)->get();

			$topCreator = User::orderBy('reputation', 'desc')->first();

	        return \View::make("frontend-blades.home")
	        	->with("categories", $categories)
	        	->with("subcategories", $subs)
	        	->with("posttypes", $types)
	        	->with("posts", $posts)
	        	->with("topCreator", $topCreator);
		}
		else
		{
			return view('frontend-blades.landing');
		}
	}

}
