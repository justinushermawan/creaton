<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Redirect;
use Auth;
use Session;
use File;
use Mail;
use URL;
use App\User;
use App\Post;
use App\Category;
use App\SubCategory;
use App\PostType;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller {

	public function login() 
	{
		$data = Input::all();

		// Remember Me
		$remember = (Input::has('remember')) ? true : false;

		// Validation Rules
		$rules = array(
			'username' => 'required|email',
			'password' => 'required|min:6',
		);

		$validator = Validator::make($data, $rules);

		if ($validator->fails())
		{
			return Redirect::to('login.php?login_attempt=1')->withInput(Input::except('password'))->withErrors($validator);
		}
		else
		{
			$userdata = array(
				'email' => Input::get('username'),
				'password' => Input::get('password')
			);

			if (Auth::validate($userdata))
			{
				if (Auth::attempt($userdata, $remember))
				{
					return Redirect::to('/');
				}
			}
			else
			{
				Session::flash('error', 'Something went wrong');
				return Redirect::to('login.php?login_attempt=1');
			}
		}
	}

	public function loginpage()
	{
		return \View::make('frontend-blades.signin');
	}

	public function signuppage()
	{
		return \View::make('frontend-blades.signup');
	}

	public function logout() 
	{
		Auth::logout();
		return Redirect::to('/');
	}

	public function forgotGet()
	{
		return \View::make('frontend-blades.forgot-password');
	}

	public function forgotPost()
	{
		$validator = Validator::make(Input::all(),
			array(
				'email' => 'required|email'
			)
		);

		if ($validator->fails()) {
			return Redirect::route('forgot-password-get')
				->withErrors($validator)
				->withInput();
		} else {
			$user = User::where('email', '=', Input::get('email'));

			if($user->count()) {
				
				$user = $user->first();

				$code = str_random(60);
				$password = str_random(10);
				
				$user->code = $code;
				$user->password_temp = Hash::make($password);

				$username = $user->username;
				
				if($user->save()) {  

					Mail::send('frontend-blades.mail-forgot', array('link'=>URL::route('account-recover', $code), 'username'=>$username, 'password'=>$password), function($message) use ($user){
						$message->to($user->email, $user->username)->subject('Your New Creaton Password');
					});
					 
					return Redirect::to('/')
						->with('We have sent you a new password by email');
				}
			}
		}

		return Redirect::route('forgot-password-get')
			->with('global', 'Could not request new password.');
	}

	public function getRecover($code) {
		$user = User::where('code', '=', $code)
				->where('password_temp', '!=', '');

		if($user->count()) {
			$user = $user->first();

			$user->password 		= $user->password_temp;
			$user->password_temp 	= '';
			$user->code 			= '';

			if($user->save()) {
				return Redirect::to('/')
					->with('Your account has been recovered and you can sign in with your password.');
			}
		}
		return Redirect::to('/')
			->with('Count not recover your account.');
	}

	public function register()
	{
		$data = Input::only(['username', 'email', 'password']);
		$data['password'] = Hash::make($data['password']);

		$newUser = User::create($data);

		// Make their own private directory
		$path = base_path() . '/userdata/' . $data['username'] . '/';
		$result = File::makeDirectory($path, $mode = 0777, true, true);

		// Make sub-folder profile picture
		$path = base_path() . '/userdata/' . $data['username'] . '/profile-picture/';
		$result = File::makeDirectory($path, $mode = 0777, true, true);

		// Make sub-folder post
		$path = base_path() . '/userdata/' . $data['username'] . '/post/';
		$result = File::makeDirectory($path, $mode = 0777, true, true);

		if ($newUser)
		{
			//Auth::login($newUser);
			return Redirect::to('login.php')->withFlashMessage('Registration Success! Please login with your new Creaton Account.');;
		}
	}

	public function profile($user_name) 
	{
		$guy = User::where('username', '=', $user_name)->firstOrFail();

		// Get All of Category Data
		$categories = Category::all();

		// Get Text Type of Sub-Category Data
		$subs = SubCategory::where('category_id', '=', 3)->where('audit_status', '!=', 'D')->get();

		// Get All of Post Type Data
		$types = PostType::all();

		// Get the Full Name
		$fullname = $guy->fullname;

		// Get title
		$title = $guy->title;

		// Get Total Posts
		$posts = Post::where('user_id', '=', $guy->id)
					 ->where('audit_status', '!=', 'D')
					 ->orderBy('created_at', 'DESC')
					 ->get();
		$totalPosts = count($posts);

		// Get Total Reputations
		$totalReps =  $guy->reputation;

		$topCreator = User::orderBy('reputation', 'desc')->first();

		return \View::make('frontend-blades.profile')
			->with("username", $user_name)
			->with("fullname", $fullname)
			->with('title', $title)
			->with("reps", $totalReps)
			->with("posts", $posts)
			->with("totalposts", $totalPosts)
			->with("categories", $categories)
			->with("subcategories", $subs)
			->with("posttypes", $types)
			->with("topCreator", $topCreator);
	}

	public function settingsGet()
	{
		$me = Auth::user();

		// Get All of Category Data
		$categories = Category::all();

		// Get Text Type of Sub-Category Data
		$subs = SubCategory::where('category_id', '=', 3)->where('audit_status', '!=', 'D')->get();

		// Get All of Post Type Data
		$types = PostType::all();

		// Get the email
		$email = $me->email;

		// Get the username
		$user_name = $me->username;

		// Get the Full Name
		$fullname = $me->fullname;

		// Get title
		$title = $me->title;

		// Get about me
		$aboutm = $me->about_me;

		// Get Total Posts
		$posts = Post::where('user_id', '=', $me->id)
					 ->where('audit_status', '!=', 'D')
					 ->orderBy('created_at', 'DESC')
					 ->get();
		$totalPosts = count($posts);

		// Get Total Reputations
		$totalReps =  $me->reputation;

		$topCreator = User::orderBy('reputation', 'desc')->first();

		return \View::make('frontend-blades.settings')
			->with("email", $email)
			->with("username", $user_name)
			->with("fullname", $fullname)
			->with('title', $title)
			->with('aboutm', $aboutm)
			->with("reps", $totalReps)
			->with("posts", $posts)
			->with("totalposts", $totalPosts)
			->with("categories", $categories)
			->with("subcategories", $subs)
			->with("posttypes", $types)
			->with("topCreator", $topCreator);
	}

	public function settingsPost()
	{
		$data = Input::all();

		$me = Auth::user();

		$me->email = $data['email'];
		$me->username = $data['username'];
		$me->fullname = $data['fullname'];
		$me->password = Hash::make($data['password']);
		$me->title = $data['title'];
		$me->about_me = $data['aboutme'];
		$me->save();

		return Redirect::to('/account/settings');
	}

}
