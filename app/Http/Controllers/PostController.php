<?php namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Post;
use App\PostReport;
use App\PostType;
use App\PostUpvote;
use App\SubCategory;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Image;
use Input;
use Redirect;
use Request;
use Response;
use Validator;
use View;

class PostController extends Controller {

	public function index(Post $post)
	{
		// Get All of Category Data
		$categories = Category::all();

		// Get Text Type of Sub-Category Data
		$subs = SubCategory::where('category_id', '=', 3)->where('audit_status', '!=', 'D')->get();

		// Get All of Post Type Data
		$types = PostType::all();

		// Get All of Post Data
		$post = Post::with('user','subcategory')
				->with('subcategory.category')
				->where('audit_status', '!=', 'D')
				->findOrFail($post->id);

		// Add total views everytime it's clicked
		$post->addTotalViews();

		$tags = explode(',', $post->tags);

		$comments = Comment::with('user')
					->whereRaw('post_id = '. $post->id)
					->where('audit_status', '!=', 'D')
					->get();

		$topCreator = User::orderBy('reputation', 'desc')->first();

        return View::make("frontend-blades.post")
        	->with("categories", $categories)
        	->with("subcategories", $subs)
        	->with("posttypes", $types)
        	->with("post", $post)
        	->with("tags", $tags)
        	->with("comments", $comments)
        	->with("topCreator", $topCreator);
	}

	/**
	 * Upload new post
	 * @return void
	 */
	public function upload()
	{
		$post = new Post;

		$post->user_id = Auth::user()->id;
		$post->post_type_id = Input::get('posttype');
		$post->subcategory_id = Input::get('category');


		switch($post->post_type_id)
		{
			// image
			case 1:

				$post->title = Input::get('title-image');
				$post->tags = Input::get('tags-image');
				$post->allow_comment = Input::get('check-comments-image') === 'yes';
				$post->nsfw = Input::get('check-nsfw-image') === 'yes';

				// Get file path from fileupload
				$files = Input::file('images');
				foreach ($files as $file)
				{
					$rules = array('file' => 'required');
					$validator = Validator::make(array('file' => $file), $rules);
					if ($validator->passes()) {
						$destinationPath = base_path('/userdata/' . Auth::user()->username . '/post/');

						$logoPath = base_path('images/watermark/25x150.png');
						//$logoImg = Image::make($logoPath);
						$postImg = Image::make($file->getRealPath());
						$postImg->insert($logoPath);


						$filename = date('YmdHis_') . $file->getClientOriginalName();

						// Fill file_path of the post
			  			$post->file_path = $filename;
			  			$postImg->save($destinationPath . $filename);
						$success = $file->move($destinationPath, $filename);

						$img = Image::make($destinationPath . $filename);
						$img->insert($logoPath);
						$img->save($destinationPath . $filename);
						
					}
					else
					{

					}
				}
				break;
			// video
			case 2: 

				$post->title = Input::get('title-video');
				$post->tags = Input::get('tags-video');
				$post->allow_comment = Input::get('check-comments-video') === 'yes';
				$post->nsfw = Input::get('check-nsfw-video') === 'yes';

				// Get video url  
				$post->video_url = Input::get('video-url');
				break;
			// text
			case 3: 

				$post->title = Input::get('title-text');
				$post->tags = Input::get('tags-text');
				$post->allow_comment = Input::get('check-comments-text') === 'yes';
				$post->nsfw = Input::get('check-nsfw-text') === 'yes';

				// Get message text
				$post->message = Input::get('message');
				break;
		}
		
		$post->slug = $post->getSlug($post->title);
		$post->reputation = 0;
		$post->audit_status = 'I';

		$post->save();
		return Redirect::to('/');
	}

	public function delete()
	{
		$post_id = Input::get('post_id');
		$sender = Input::get('src');
		$post = Post::findOrFail($post_id);
		$post->audit_status = 'D';

		if($post->save())
		{
			return url();
		}
		else
		{
			return Response::json(array('success' => false), 400);
		}
	}

	/**
	 * Upvote a post
	 * @return void
	 */
	public function upvote()
	{
		if(Request::ajax())
		{
			$upvote = new PostUpvote;

			$upvote->user_id = Input::get('user_id');
			$upvote->post_id = Input::get('post_id');
			$upvote->audit_status = 'I';

			$post = Post::find(Input::get('post_id'));
			$post->reputation += 1;
			$post->save();

			if($upvote->save())
			{
				return $post->reputation;
			}
			else
			{
				return Response::json(array('success' => false), 400);
			}
		}
	}

	/**
	 * Downvote/cancel upvote a post
	 * @return void
	 */
	public function downvote()
	{
		if(Request::ajax())
		{
			$upvote = new PostUpvote;

			$user_id = Input::get('user_id');
			$post_id = Input::get('post_id');

			$success = DB::table('post_upvotes')
							->where('user_id', '=', $user_id)
							->where('post_id', '=', $post_id)
							->where('audit_status', "!=", 'D')
							->delete();

			$post = Post::find(Input::get('post_id'));
			$post->reputation -= 1;
			$post->save();

			if($success)
			{
				return $post->reputation;
			}
			else
			{
				return Response::json(array('success' => false), 400);
			}
		}
	}

	public function search($key)
	{
		// Get All of Post Type Data
		$types = PostType::all();
		
		// Get Text Type of Sub-Category Data
		$subs = SubCategory::where('category_id', '=', 3)->where('audit_status', '!=', 'D')->get();

		$keyWild = '%'.$key.'%';

		$posts = Post::with('user', 'subcategory')
				->with('subcategory.category')
				->where(function($query) use ($keyWild) {
					$query->where('title', 'LIKE', $keyWild)
						  ->orWhere('message', 'LIKE', $keyWild)
						  ->orWhere('tags', 'LIKE', $keyWild);
				})
				->where('audit_status', '!=', 'D')
				->get();

		$users = null;

		$searchCount = count($posts);
		// $post1 = Post::where('title', 'LIKE', $keyWild)->where('audit_status', '!=', 'D')->get();
		// $post2 = Post::where('message', 'LIKE', $keyWild)->where('audit_status', '!=', 'D')->get();
		// $posts = array_merge((array) $post1, (array) $post2);

		return View::make('frontend-blades.search')
				   ->with('key', $key)
				   ->with('posttypes', $types)
				   ->with('subcategories', $subs)
				   ->with('posts', $posts)
				   ->with('users', $users)
				   ->with('searchCount', $searchCount);
	}

	public function advanced_search()
	{
		// Get All of Post Type Data
		$types = PostType::all();
		
		// Get Text Type of Sub-Category Data
		$subs = SubCategory::where('category_id', '=', 3)->where('audit_status', '!=', 'D')->get();

		$key = Input::get('key');
		$searchBy = Input::get('searchby'); // 0->post; 1->creator
		$orderBy = Input::get('orderby') == 0 ? 'created_at' : 'reputation'; // 0->created_at; 1->reputation
		$orderType = Input::get('ordertype') == 0 ? 'asc' : 'desc'; // 0->ascending; 1->descending

		$keyWild = '%'.$key.'%';

		$posts = null;
		$users = null;
		$searchCount = 0;

		if($searchBy == 0)
		{
			// search post
			$posts = Post::with('user', 'subcategory')
				->with('subcategory.category')
				->where(function ($query) use ($keyWild) {
					$query->where('title', 'LIKE', $keyWild)
						  ->orWhere('message', 'LIKE', $keyWild)
						  ->orWhere('tags', 'LIKE', $keyWild);
				})
				->where('audit_status', '!=', 'D')
				->orderBy($orderBy, $orderType)
				->get();
			$searchCount = count($posts);
		}
		else if($searchBy == 1)
		{
			//search creator
			$users = User::where(function ($query) use ($keyWild){
					$query->where('username', 'LIKE', $keyWild)
						  ->orWhere('fullname', 'LIKE', $keyWild)
						  ->orWhere('title', 'LIKE', $keyWild);
				})
				->where('audit_status', '!=', 'D')
				->orderBy($orderBy, $orderType)
				->get();
			$searchCount = count($users);
		}

		return View::make('frontend-blades.search')
				   ->with('key', $key)
				   ->with('posttypes', $types)
				   ->with('subcategories', $subs)
				   ->with('posts', $posts)
				   ->with('users', $users)
				   ->with('searchCount', $searchCount)
				   ->with('searchBy', $searchBy)
			       ->with('orderBy', $orderBy)
			       ->with('orderType', $orderType)
				   ;
	}

	public function report()
	{
		if(Request::ajax())
		{
			$report 				= new PostReport;
			$report->user_id 		= Auth::user()->id;
			$report->post_id 		= Input::get('post_id');
			$report->reason 		= Input::get('reason');
			$report->description 	= Input::get('description');
			$report->audit_status 	= 'I';

			if($report->save())
			{
				return url();
			}
			else
			{
				return Response::json(array('success' => false), 400);
			}
		}
	}

	public function getSubcategory($id)
	{
		if(Request::ajax())
		{
			$subs = SubCategory::where('category_id', '=', $id)->where('audit_status', '!=', 'D')->get();

			return $subs->toJson();
		}
	}

}
