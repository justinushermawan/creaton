<?php

use App\Category;
use App\Post;
use App\SubCategory;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Landing page
Route::get('/', 'LandingController@index');

Route::get('post/{post_slug}', 'PostController@index');

Route::bind('post_slug', function($value, $route) {
	return Post::where('slug', $value)->firstOrFail();
});

// Single Login Page
Route::get('login.php', 'AccountController@loginpage');

// Single Sign Up Page
Route::get('r.php', 'AccountController@signuppage');

// Home page (after login)
Route::get('home', 'HomeController@index');

// Post detail page
Route::get('post/{id}', 'PostController@index');

// Load more post (Home page)
Route::post('postmore', 'HomeController@loadMore');

// Upload new post
Route::post('post/upload', 'PostController@upload');

// Delete a post
Route::post('deletepost', 'PostController@delete');
Route::post('post/deletepost', 'PostController@delete');

// Upvote a post
Route::post('upvotePost', 'PostController@upvote');
Route::post('post/upvotePost', 'PostController@upvote');

// Downvote a post
Route::post('downvotePost', 'PostController@downvote');
Route::post('post/downvotePost', 'PostController@downvote');

// Search post
Route::get('search/{key}', ['uses' => 'PostController@search']);

// Search post & creator
Route::post('advanced_search', 'PostController@advanced_search');

// Report a post
Route::post('reportPost', 'PostController@report');
Route::post('post/reportPost', 'PostController@report');

// Post a comment
Route::post('post/postcomment', 'CommentController@post');

// Delete a comment
Route::post('post/deletecomment', 'CommentController@delete');

// Upvote a comment
Route::post('post/upvoteComment', 'CommentController@upvote');

// Downvote a comment
Route::post('post/downvoteComment', 'CommentController@downvote');

// POST Login
Route::post('account/login', 'AccountController@login');

// GET Logout
Route::get('account/logout', array('uses' => 'AccountController@logout'));

// POST Register
Route::post('account/register', 'AccountController@register');

// ==============================================================
//                       Forgot Password
// ==============================================================
// GET
Route::get('/login/recover',
	array('as' => 'forgot-password-get',
		'uses' => 'AccountController@forgotGet'
));
// POST
Route::post('/login/recover',
	array('as' => 'forgot-password-post',
		'uses' => 'AccountController@forgotPost'
));
// RECOVER
Route::get('/account/recover/{code}',
	array('as' => 'account-recover',
		'uses' => 'AccountController@getRecover'
));
// ==============================================================
//                    End of Forgot Password
// ==============================================================

// POST Upload
Route::post('post/upload', 'PostController@upload');

Route::post('comment/post', 'CommentController@post');

// GET Login
Route::get('account/login', function() {
  return View::make('login');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// Get Subcategory on Post Modal
Route::get('getSubcategory/{id}', ['uses' => 'PostController@getSubcategory']);

// Creator Profile
Route::get('/{user_name}', array('uses' => 'AccountController@profile'));

// Category Page
Route::get('/c/{slugCat}', ['uses' => 'CategoryController@show']);

// SubCategory Page
Route::get('/sc/{slugSub}', ['uses' => 'CategoryController@showSub']);

// GET Settings
Route::get('/account/settings', array('uses' => 'AccountController@settingsGet'));

// POST Settings
Route::post('/account/settings', array('uses' => 'AccountController@settingsPost'));