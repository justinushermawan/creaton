<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Post extends Model {

	protected $table = 'posts';

	protected $fillable = ['slug', 'user_id', 'post_type_id', 'subcategory_id', 'title', 'message', 'file_path', 'video_url', 'tags', 'allow_comment', 'nsfw', 'reputation', 'total_views','audit_status'];

	protected $guarded = ['id'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function subcategory()
	{
		return $this->belongsTo('App\SubCategory');
	}

	public function comments()
	{
		return $this->hasMany('App\Comment');
	}

	public function commentCount()
	{
		return $this->hasMany('App\Comment')->where('audit_status', '!=', 'D')->count();
	}

	public function isVoted($user_id)
	{
		return DB::table('post_upvotes')
					->where('post_id', '=', $this->id)
					->where('user_id', '=', $user_id)
					->count();
	}

	public function addTotalViews()
	{
		$this->total_views += 1;
		$this->save();
		return;
	}

	public function getSlug($title)
	{
		$slug = Str::slug($title);
		$slugCount = count( $this->whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'")->get() );

		return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
	}

	public function isReportedBy($user_id)
	{
		return DB::table('post_reports')->where('post_id', '=', $this->id)->where('user_id', '=', $user_id)->count();
	}

}
