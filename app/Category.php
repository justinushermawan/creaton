<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = 'categories';

	protected $fillable = ['slug', 'name', 'color', 'audit_status'];

	protected $guarded = ['id'];

	public function subcategories()
	{
		return $this->hasMany('App\SubCategory');
	}

}
