<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentUpvote extends Model {

	protected $table = 'comment_upvotes';

	protected $fillable = ['user_id', 'comment_id', 'audit_status'];

	protected $guard = ['id'];

}
