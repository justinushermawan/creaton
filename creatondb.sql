-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2015 at 09:55 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `creatondb`
--
CREATE DATABASE IF NOT EXISTS `creatoni_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `creatoni_db`;

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE IF NOT EXISTS `administrators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `administrators_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`id`, `username`, `password`, `name`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$24pwDO5eMX.EmMk10tmPWulo93Bk//Q5r3LetusjrICdonqomePLG', 'SleepingOwl Administrator', 'xFTOujP0UpY1Mgd6e1w7rIm9DXBQSDCDjyUdaD5nLbT65KhMYQr3aYoggJHc', '2015-05-30 12:05:49', '2015-06-01 09:01:11');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `audit_status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `slug`, `name`, `color`, `audit_status`, `created_at`, `updated_at`) VALUES
(1, 'image', 'Images', 'blue', 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(2, 'video', 'Videos', 'red', 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(3, 'text', 'Texts', 'green', 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `reputation` int(11) NOT NULL,
  `audit_status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `comments_user_id_foreign` (`user_id`),
  KEY `comments_post_id_foreign` (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `content`, `reputation`, `audit_status`, `created_at`, `updated_at`) VALUES
(1, 1, 7, 'Great artwork, sir! Keep up your great job :)', 0, 'I', '2015-05-25 09:08:30', '2015-05-25 09:08:30'),
(2, 1, 10, 'Wow such NSFW', 1, 'D', '2015-05-26 08:55:52', '2015-05-29 01:55:00'),
(3, 1, 10, '', 1, 'D', '2015-05-26 09:20:23', '2015-05-29 01:28:52'),
(4, 1, 10, '', 1, 'D', '2015-05-26 09:21:00', '2015-05-29 01:29:23'),
(5, 1, 10, 'test', 1, 'D', '2015-05-26 10:13:55', '2015-05-29 01:31:25'),
(6, 1, 10, 'test', 0, 'D', '2015-05-26 10:14:42', '2015-05-29 01:33:52'),
(7, 1, 10, 'test ajax', 0, 'D', '2015-05-26 10:15:37', '2015-05-29 01:36:19'),
(8, 1, 10, 'test ajax', 0, 'D', '2015-05-26 10:15:52', '2015-05-29 01:39:10'),
(9, 1, 10, 'testtest', 0, 'D', '2015-05-26 10:49:25', '2015-05-29 01:40:38'),
(10, 1, 10, 'testtest', 0, 'D', '2015-05-26 10:50:15', '2015-05-29 01:39:26'),
(11, 1, 10, 'test 1', 0, 'D', '2015-05-26 10:50:47', '2015-05-29 01:42:54'),
(12, 1, 10, 'test 2', 0, 'D', '2015-05-26 20:02:56', '2015-05-29 01:52:37'),
(13, 1, 10, 'test 3', 0, 'D', '2015-05-26 20:05:03', '2015-05-29 01:54:00'),
(14, 1, 10, 'test 4', 0, 'D', '2015-05-26 20:24:57', '2015-05-29 01:54:44'),
(15, 1, 10, 'test 5', 0, 'D', '2015-05-26 20:29:00', '2015-05-29 23:38:26'),
(16, 1, 10, 'test 6', 0, 'D', '2015-05-26 20:36:16', '2015-05-29 23:40:05'),
(17, 1, 10, 'test 7', 0, 'D', '2015-05-26 20:37:54', '2015-05-29 23:41:30'),
(18, 1, 10, 'test 8', 0, 'D', '2015-05-26 20:40:53', '2015-05-30 01:47:37'),
(19, 1, 10, 'test 9', 0, 'I', '2015-05-26 20:42:07', '2015-05-26 20:42:07'),
(20, 1, 10, 'test 10', 0, 'I', '2015-05-26 20:56:45', '2015-05-26 20:56:45'),
(21, 1, 10, 'test 11', 0, 'I', '2015-05-26 21:01:35', '2015-05-26 21:01:35'),
(22, 1, 10, 'test 12', 0, 'I', '2015-05-26 21:07:40', '2015-05-26 21:07:40'),
(23, 1, 10, 'test 13', 0, 'D', '2015-05-26 21:11:01', '2015-05-29 01:55:06'),
(24, 1, 10, 'test 13', 0, 'I', '2015-05-29 02:11:03', '2015-05-29 02:11:03'),
(25, 1, 10, 'test 14', 0, 'I', '2015-05-29 02:12:12', '2015-05-29 02:12:12'),
(26, 1, 10, 'test 15', 0, 'I', '2015-05-29 02:13:30', '2015-05-29 02:13:30'),
(27, 1, 10, 'test 16', 0, 'I', '2015-05-29 02:28:57', '2015-05-29 02:28:57'),
(28, 1, 10, 'test 17', 0, 'I', '2015-05-29 02:32:05', '2015-05-29 02:32:05'),
(29, 1, 10, 'test 18', 0, 'I', '2015-05-29 02:49:04', '2015-05-29 02:49:04'),
(30, 1, 10, 'test 19', 0, 'I', '2015-05-29 02:52:02', '2015-05-29 02:52:02'),
(31, 1, 10, 'test 20', 0, 'I', '2015-05-29 02:53:24', '2015-05-29 02:53:24'),
(32, 1, 28, 'test', 1, 'D', '2015-05-30 04:58:05', '2015-05-30 05:00:58');

-- --------------------------------------------------------

--
-- Table structure for table `comment_upvotes`
--

CREATE TABLE IF NOT EXISTS `comment_upvotes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `comment_id` int(10) unsigned NOT NULL,
  `audit_status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`),
  KEY `comment_upvotes_user_id_foreign` (`user_id`),
  KEY `comment_upvotes_comment_id_foreign` (`comment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `comment_upvotes`
--

INSERT INTO `comment_upvotes` (`id`, `user_id`, `comment_id`, `audit_status`, `created_at`, `updated_at`) VALUES
(9, 1, 3, '', '2015-05-28 09:28:26', '2015-05-28 09:28:26'),
(10, 1, 2, '', '2015-05-28 09:28:32', '2015-05-28 09:28:32'),
(11, 1, 4, '', '2015-05-28 20:35:03', '2015-05-28 20:35:03'),
(12, 1, 5, '', '2015-05-28 23:54:21', '2015-05-28 23:54:21'),
(13, 1, 32, '', '2015-05-30 05:00:53', '2015-05-30 05:00:53');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `audit_status` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `messages_sender_id_foreign` (`sender_id`),
  KEY `messages_recipient_id_foreign` (`recipient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_05_15_000001_create_users_table', 1),
('2015_05_15_000002_create_password_resets_table', 1),
('2015_05_15_000003_create_categories_table', 1),
('2015_05_15_000004_create_subcategories_table', 1),
('2015_05_15_000005_create_post_type_table', 1),
('2015_05_15_000006_create_posts_table', 1),
('2015_05_15_000007_create_admins_table', 1),
('2015_05_15_000008_create_comments_table', 1),
('2015_05_15_000009_create_upvote_comment_table', 1),
('2015_05_15_000010_create_post_reports_table', 1),
('2015_05_15_000011_create_messages_table', 1),
('2015_05_15_000012_create_post_upvotes_table', 1),
('2015_05_15_000013_create_report_users_table', 1),
('2015_05_15_000014_create_suspend_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `post_type_id` int(10) unsigned NOT NULL,
  `subcategory_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `allow_comment` tinyint(1) NOT NULL,
  `nsfw` tinyint(1) NOT NULL,
  `reputation` int(11) NOT NULL,
  `total_views` int(11) NOT NULL,
  `audit_status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `posts_user_id_foreign` (`user_id`),
  KEY `posts_post_type_id_foreign` (`post_type_id`),
  KEY `posts_subcategory_id_foreign` (`subcategory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `slug`, `user_id`, `post_type_id`, `subcategory_id`, `title`, `message`, `file_path`, `video_url`, `tags`, `allow_comment`, `nsfw`, `reputation`, `total_views`, `audit_status`, `created_at`, `updated_at`) VALUES
(6, 'avengers-age-of-ultron', 1, 2, 5, 'Avengers: Age of Ultron [Now Playing]', '', '20150522064330_avengers_aou.jpg', '', '', 0, 0, 0, 0, 'I', '2015-05-21 16:43:31', '2015-05-21 16:43:31'),
(7, 'iron-man-modelling', 1, 3, 5, 'Iron Man Modelling (3DS Max)', '', '', 'https://www.youtube.com/watch?v=pFayTbWdZAo', '3dsMax', 0, 0, 0, 0, 'I', '2015-05-23 00:11:37', '2015-05-23 00:11:37'),
(8, 'platformer-game', 1, 3, 4, 'Platformer Game (Unity 3D)', '', '', 'https://www.youtube.com/watch?v=N10nzviJQZc', 'Unity3D', 0, 0, 1, 1, 'I', '2015-05-23 00:56:02', '2015-05-30 22:10:24'),
(10, 'test-nsfw', 1, 1, 7, 'Test NSFW', 'This post may contains explicit messages.', '', '', 'nsfw,yolo', 1, 1, 4, 1, 'I', '2015-05-25 09:13:57', '2015-05-30 22:10:39'),
(11, NULL, 1, 1, 8, 'Test delete', 'Test setsetsetset', '', '', '', 0, 0, 0, 0, 'D', '2015-05-28 23:46:11', '2015-05-28 23:48:04'),
(12, NULL, 1, 1, 8, 'Test delete', 'testsetset', '', '', '', 0, 0, 0, 0, 'D', '2015-05-28 23:50:26', '2015-05-28 23:50:50'),
(13, NULL, 1, 1, 8, 'Test delete again', 'testsetsetsetsetsetsetsetsetset', '', '', '', 0, 0, 1, 0, 'D', '2015-05-28 23:52:09', '2015-05-28 23:53:33'),
(14, NULL, 1, 1, 7, 'Test delete', 'testsetset', '', '', '', 1, 0, 0, 0, 'D', '2015-05-29 22:23:58', '2015-05-29 22:27:34'),
(15, NULL, 1, 1, 7, 'Test delete', 'testsetset', '', '', '', 0, 0, 0, 0, 'D', '2015-05-29 22:29:06', '2015-05-29 22:29:17'),
(16, NULL, 1, 1, 7, 'Test delete', 'testtesttest', '', '', '', 0, 0, 0, 0, 'D', '2015-05-29 22:31:16', '2015-05-29 22:31:28'),
(17, NULL, 1, 1, 7, 'Test delete', 'testtesttest', '', '', '', 0, 0, 0, 0, 'D', '2015-05-29 22:37:19', '2015-05-29 22:37:27'),
(19, NULL, 1, 1, 7, 'Test delete', 'test', '', '', '', 0, 0, 0, 0, 'D', '2015-05-29 23:24:39', '2015-05-29 23:25:54'),
(20, NULL, 1, 1, 8, 'Test delete', 'tetestsetsetset', '', '', '', 0, 0, 0, 0, 'D', '2015-05-29 23:26:18', '2015-05-29 23:26:26'),
(21, NULL, 1, 1, 7, 'Test delete', 'tetstsetste', '', '', '', 0, 0, 0, 0, 'D', '2015-05-30 00:02:15', '2015-05-30 00:02:22'),
(22, NULL, 1, 1, 7, 'Test delete', 'testsetsets', '', '', '', 0, 0, 0, 0, 'D', '2015-05-30 01:28:46', '2015-05-30 01:29:02'),
(23, NULL, 1, 1, 8, 'Test delete', 'testsetste', '', '', '', 0, 0, 0, 0, 'D', '2015-05-30 01:30:23', '2015-05-30 01:30:30'),
(25, NULL, 1, 1, 7, 'Test delete', 'testtesttest', '', '', '', 0, 0, 0, 0, 'D', '2015-05-30 01:32:16', '2015-05-30 01:33:05'),
(26, NULL, 1, 1, 8, 'Test delete', 'test', '', '', '', 0, 0, 0, 0, 'D', '2015-05-30 01:38:24', '2015-05-30 01:38:31'),
(27, NULL, 1, 1, 7, 'Test delete', 'test', '', '', '', 0, 0, 0, 0, 'D', '2015-05-30 01:39:46', '2015-05-30 01:39:52'),
(28, 'test-post', 1, 1, 7, 'Test post', 'Tsetsdfsdfsdf', '', '', 'test,anything', 1, 0, 1, 1, 'I', '2015-05-30 02:15:35', '2015-05-30 07:44:11'),
(29, 'test-slug', 1, 1, 5, 'Test slug', 'slugggg', '', '', '', 0, 0, 0, 10, 'I', '2015-05-30 05:29:34', '2015-05-31 08:47:01'),
(31, 'test-delete-again', 1, 1, 7, 'Test delete again', 'asdasdasdasd', '', '', '', 0, 0, 0, 1, 'D', '2015-05-31 07:47:21', '2015-05-31 07:47:37'),
(34, 'test-delete-again-1', 1, 1, 8, 'Test delete again', 'qwerty', '', '', '', 0, 0, 0, 1, 'D', '2015-05-31 08:09:31', '2015-05-31 08:10:19'),
(35, 'test-slug-1', 1, 2, 3, 'test slug', '', '20150531155007_Desert.jpg', '', 'beautiful,desert', 1, 0, 1, 4, 'I', '2015-05-31 08:50:07', '2015-05-31 09:21:07');

-- --------------------------------------------------------

--
-- Table structure for table `post_reports`
--

CREATE TABLE IF NOT EXISTS `post_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `audit_status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `post_reports_user_id_foreign` (`user_id`),
  KEY `post_reports_post_id_foreign` (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `post_reports`
--

INSERT INTO `post_reports` (`id`, `user_id`, `post_id`, `reason`, `description`, `audit_status`, `created_at`, `updated_at`) VALUES
(1, 1, 29, 'This post is reposted.', 'test', 'I', '2015-05-31 07:08:21', '2015-05-31 07:08:21'),
(2, 1, 28, 'This post is spam.', 'Look at the text. What a spammy spam.', 'I', '2015-05-31 08:20:44', '2015-05-31 08:20:44'),
(3, 1, 35, 'This post insulting specific race, religion, or ethnics.', 'I am asian and i am insulted with this image!', 'I', '2015-05-31 08:55:39', '2015-05-31 08:55:39');

-- --------------------------------------------------------

--
-- Table structure for table `post_types`
--

CREATE TABLE IF NOT EXISTS `post_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `audit_status` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `post_types`
--

INSERT INTO `post_types` (`id`, `name`, `audit_status`, `created_at`, `updated_at`) VALUES
(1, 'Text Post', 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(2, 'Image Post', 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(3, 'Video Post', 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02');

-- --------------------------------------------------------

--
-- Table structure for table `post_upvotes`
--

CREATE TABLE IF NOT EXISTS `post_upvotes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `audit_status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `post_upvotes_user_id_foreign` (`user_id`),
  KEY `post_upvotes_post_id_foreign` (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `post_upvotes`
--

INSERT INTO `post_upvotes` (`id`, `user_id`, `post_id`, `audit_status`, `created_at`, `updated_at`) VALUES
(11, 1, 8, 'I', '2015-05-28 01:57:10', '2015-05-28 01:57:10'),
(17, 1, 13, 'I', '2015-05-28 23:52:22', '2015-05-28 23:52:22'),
(18, 1, 28, 'I', '2015-05-30 04:58:17', '2015-05-30 04:58:17'),
(20, 1, 35, 'I', '2015-05-31 08:54:29', '2015-05-31 08:54:29');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `audit_status` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `subcategories_category_id_foreign` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `slug`, `name`, `category_id`, `audit_status`, `created_at`, `updated_at`) VALUES
(1, 'anime-manga', 'Anime & Manga', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(2, 'cartoons-comics', 'Cartoons & Comics', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(3, 'drawings', 'Drawings', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(4, 'cosplays', 'Cosplays', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(5, 'contests', 'Contests', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(6, 'digital-art', 'Digital Art', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(7, 'fan-art', 'Fan Art', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(8, 'flash', 'Flash', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(9, 'foods', 'Foods', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(10, 'gifs', 'GIFs', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(11, 'memes', 'Memes', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(12, 'photography', 'Photography', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(13, 'street-art', 'Street Art', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(14, 'traditional-art', 'Traditional Art', 1, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(15, 'animals', 'Animals', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(16, 'covers', 'Covers', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(17, 'dance', 'Dance', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(18, 'hobbies', 'Hobbies', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(19, 'horror', 'Horror', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(20, 'humour', 'Humour', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(21, 'lets-play', 'Lets Play', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(22, 'original-song', 'Original Song', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(23, 'sports', 'Sports', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(24, 'stop-motions', 'Stop Motions', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(25, 'tutorials', 'Tutorials', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(26, 'vlogs', 'Vlogs', 2, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(27, 'humour-stories', 'Humour Stories', 3, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(28, 'journals', 'Journals', 3, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(29, 'literatures', 'Literatures', 3, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(30, 'news', 'News', 3, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(31, 'novels', 'Novels', 3, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(32, 'quotes', 'Quotes', 3, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02'),
(33, 'short-stories', 'Short Stories', 3, 'I', '2015-05-25 08:42:02', '2015-05-25 08:42:02');

-- --------------------------------------------------------

--
-- Table structure for table `suspend_users`
--

CREATE TABLE IF NOT EXISTS `suspend_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `suspend_start_time` datetime NOT NULL,
  `suspend_end_time` datetime NOT NULL,
  `audit_status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `suspend_users_admin_id_foreign` (`admin_id`),
  KEY `suspend_users_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password_temp` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gender` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reputation` int(11) NOT NULL,
  `profile_picture_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about_me` text COLLATE utf8_unicode_ci NOT NULL,
  `audit_status` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `fullname`, `email`, `password`, `password_temp`, `code`, `gender`, `title`, `reputation`, `profile_picture_url`, `about_me`, `audit_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'vdanny', 'Vinsensius Danny', 'vinsensius.danny@gmail.com', '$2y$10$qzaCfWsQEOEhUgio1QtO6OrzKePZavZlsogqGTiyx/3uGyvRsWznG', '$2y$10$9JjWSj2B2TcZ3IkM7KWS9e/kuYB41BhCwK4XFrW4y31.oBl8dHN7O', 'ObhSed6bUNhD0YFnGpahehsTg7J4w6rqxVIUYs53pmDkrr7B86KCIwVh1phQ', 'M', 'Web Developer', 99, '', 'first user ever', 'I', 'ggXeqKPOqMcL6fjD7t4izsph5JA6I9ApVDzpGZMnR8iztamChZgagnGUUjW7', '2015-05-25 08:42:02', '2015-06-01 07:05:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_reports`
--

CREATE TABLE IF NOT EXISTS `user_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reporter_id` int(10) unsigned NOT NULL,
  `reported_id` int(10) unsigned NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `audit_status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `report_users_reporter_id_foreign` (`reporter_id`),
  KEY `report_users_reported_id_foreign` (`reported_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_settings`
--

CREATE TABLE IF NOT EXISTS `user_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `show_nsfw` tinyint(1) NOT NULL,
  `notify_comment` tinyint(1) NOT NULL,
  `notify_upvote` tinyint(1) NOT NULL,
  `notify_downvote` tinyint(1) NOT NULL,
  `notify_report` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comment_upvotes`
--
ALTER TABLE `comment_upvotes`
  ADD CONSTRAINT `comment_upvotes_comment_id_foreign` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`),
  ADD CONSTRAINT `comment_upvotes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_recipient_id_foreign` FOREIGN KEY (`recipient_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `messages_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_post_type_id_foreign` FOREIGN KEY (`post_type_id`) REFERENCES `post_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_reports`
--
ALTER TABLE `post_reports`
  ADD CONSTRAINT `post_reports_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_upvotes`
--
ALTER TABLE `post_upvotes`
  ADD CONSTRAINT `post_upvotes_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_upvotes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `suspend_users`
--
ALTER TABLE `suspend_users`
  ADD CONSTRAINT `suspend_users_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `administrators` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `suspend_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_reports`
--
ALTER TABLE `user_reports`
  ADD CONSTRAINT `report_users_reported_id_foreign` FOREIGN KEY (`reported_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `report_users_reporter_id_foreign` FOREIGN KEY (`reporter_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_settings`
--
ALTER TABLE `user_settings`
  ADD CONSTRAINT `user_settings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
