<?php

use App\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder {

	public function run()
	{
		DB::table('categories')->delete();

		Category::create(
			[
				'name'                  => 'Images',
				'audit_status'          => 'I'
			]
		);
		Category::create(
			[
				'name'                  => 'Videos',
				'audit_status'          => 'I'
			]
		);
		Category::create(
			[
				'name'                  => 'Texts',
				'audit_status'          => 'I'
			]
		);
	}

}