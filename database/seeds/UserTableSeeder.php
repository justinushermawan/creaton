<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

		User::create([
			'username'              => 'vdanny',
			'fullname'				=> 'Vinsensius Danny',
			'email'                 => 'vinsensius.danny@gmail.com',
			'password'              => bcrypt('password'),
			'gender'                => 'M',
			'title'              	=> 'Web Developer',
			'reputation'            => 99,
			'profile_picture_url'   => '',
			'about_me'              => 'first user ever',
			'audit_status'          => 'I'
		]);
	}

}