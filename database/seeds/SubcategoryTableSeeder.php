<?php

use App\SubCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubcategoryTableSeeder extends Seeder {

	public function run()
	{
		DB::table('subcategories')->delete();


		SubCategory::create(
			[
				'name'                  => 'Manga',
				'category_id'			=> 1,
				'audit_status'          => 'I'
			]
		);
		SubCategory::create(
			[
				'name'                  => 'Cartoons',
				'category_id'			=> 1,
				'audit_status'          => 'I'
			]
		);
		SubCategory::create(
			[
				'name'                  => 'Hand drawings',
				'category_id'			=> 1,
				'audit_status'          => 'I'
			]
		);
		SubCategory::create(
			[
				'name'                  => 'Stop Motion',
				'category_id'			=> 2,
				'audit_status'          => 'I'
			]
		);
		SubCategory::create(
			[
				'name'                  => 'Film & Animations',
				'category_id'			=> 2,
				'audit_status'          => 'I'
			]
		);
		SubCategory::create(
			[
				'name'                  => 'Anime',
				'category_id'			=> 2,
				'audit_status'          => 'I'
			]
		);
		SubCategory::create(
			[
				'name'                  => 'Literatures',
				'category_id'			=> 3,
				'audit_status'          => 'I'
			]
		);
		SubCategory::create(
			[
				'name'                  => 'Poetry',
				'category_id'			=> 3,
				'audit_status'          => 'I'
			]
		);
		SubCategory::create(
			[
				'name'                  => 'Songs & Lyrics',
				'category_id'			=> 3,
				'audit_status'          => 'I'
			]
		);
	}

}