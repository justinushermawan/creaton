<?php

use App\PostType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostTypeTableSeeder extends Seeder {

	public function run()
	{
		DB::table('post_types')->delete();

		PostType::create(
			[
				'name'                  => 'Text Post',
				'audit_status'          => 'I'
			]
		);
		PostType::create(
			[
				'name'                  => 'Image Post',
				'audit_status'          => 'I'
			]
		);
		PostType::create(
			[
				'name'                  => 'Video Post',
				'audit_status'          => 'I'
			]
		);
	}

}