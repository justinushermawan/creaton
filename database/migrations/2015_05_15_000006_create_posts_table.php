<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slug');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('post_type_id')->unsigned();
            $table->foreign('post_type_id')->references('id')->on('post_types')->onDelete('cascade');
            $table->integer('subcategory_id')->unsigned();
            $table->foreign('subcategory_id')->references('id')->on('subcategories')->onDelete('cascade');
            $table->string('title');
            $table->string('message'); // if post type is text
            $table->string('file_path'); // if post type is image & audio
            $table->string('video_url'); // if post type is video
            $table->string('tags');
            $table->boolean('allow_comment');
            $table->boolean('nsfw');
            $table->integer('reputation');
            $table->integer('total_views');
            $table->char('audit_status', 1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
