<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('reporter_id')->unsigned();
			$table->foreign('reporter_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('reported_id')->unsigned();
			$table->foreign('reported_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('reason');
			$table->text('description');
			$table->char('audit_status', 1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('report_users');
	}

}
